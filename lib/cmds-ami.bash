# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
declare source_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

source "$VAARK_AWS_LIB_DIR/utils.bash"

# https://gist.github.com/gdamjan/09bf59445a6ef3c4c99933c2c0374560#file-latest-ami-sh
# Get the AMI id of the latest official Debian/Fedora/Ubuntu images (hvm-x86_64-gp2)
# the new AWS account for Debian will be 136693071363 from late 2019 on

##
# additional filters:
#  'Name=name,Values=debian-stretch-*'
#  'Name=architecture,Values=x86_64'
#  'Name=virtualization-type,Values=hvm'
#

# default users
# https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/connection-prereqs.html

function aws-sys-ami-latest-debian-10() {
  declare region=${1:-${AWS_REGION:-${AWS_DEFAULT_REGION:-$(aws configure get region)}}}
  # Debian-10
  aws ec2 describe-images \
      --region $region \
      --output text --query 'sort_by(Images, &CreationDate)[-1].ImageId' \
      --owners 136693071363 \
      --filters Name=name,Values='debian-10-amd64-*' \
#
} # aws-sys-ami-latest-debian-10()

function aws-sys-ami-latest-debian-11() {
  declare region=${1:-${AWS_REGION:-${AWS_DEFAULT_REGION:-$(aws configure get region)}}}
  # Debian-11
  aws ec2 describe-images \
      --region $region \
      --output text --query 'sort_by(Images, &CreationDate)[-1].ImageId' \
      --owners 136693071363 \
     --filters Name=name,Values='debian-11-amd64-*' \
#
} # aws-sys-ami-latest-debian-11()

function aws-sys-ami-latest-ubuntu-20() {
  declare region=${1:-${AWS_REGION:-${AWS_DEFAULT_REGION:-$(aws configure get region)}}}
  # Ubuntu-20:
  aws ec2 describe-images \
      --region $region \
      --output text --query 'sort_by(Images, &CreationDate)[-1].ImageId' \
      --owners 099720109477 \
      --filters Name=name,Values='ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*' \
#
} # aws-sys-ami-latest-ubuntu-20()

function aws-sys-ami-latest-ubuntu-22() {
  declare region=${1:-${AWS_REGION:-${AWS_DEFAULT_REGION:-$(aws configure get region)}}}
  # Ubuntu-22:
  aws ec2 describe-images \
      --region $region \
      --output text --query 'sort_by(Images, &CreationDate)[-1].ImageId' \
      --owners 099720109477 \
      --filters Name=name,Values='ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*' \
#
} # aws-sys-ami-latest-ubuntu-22()

function aws-sys-ami-latest-fedora-34() {
  declare region=${1:-${AWS_REGION:-${AWS_DEFAULT_REGION:-$(aws configure get region)}}}

  # Fedora-34
  aws ec2 describe-images \
      --region $region \
      --output text --query 'sort_by(Images, &CreationDate)[-1].ImageId' \
      --owners 125523088429 \
      --filters Name=name,Values='Fedora-Cloud-Base-34-*x86_64-hvm-*-gp2-*' \
#
} # aws-sys-ami-latest-fedora-34()

function aws-sys-ami-latest-fedora-35() {
  declare region=${1:-${AWS_REGION:-${AWS_DEFAULT_REGION:-$(aws configure get region)}}}

  # Fedora-35
  aws ec2 describe-images \
      --region $region \
      --output text --query 'sort_by(Images, &CreationDate)[-1].ImageId' \
      --owners 125523088429 \
      --filters Name=name,Values='Fedora-Cloud-Base-35-*x86_64-hvm-*-gp2-*' \
#
} # aws-sys-ami-latest-fedora-35()

function aws-image-id() {
  declare image_owner=$1 image_filter=$2 region=$3
  aws ec2 describe-images --owners $image_owner --region $region --output text --query 'sort_by(Images, &CreationDate)[-1].ImageId' --filters Name=name,Values="$image_filter"
} # aws-image-id()

function aws-sys-ami-latest() {
  declare -a images=( $@ )
  declare -A func_from_image=(
    [debian-10]="aws-sys-ami-latest-debian-10"
    [debian-11]="aws-sys-ami-latest-debian-11"

    [ubuntu-20]="aws-sys-ami-latest-ubuntu-20"
    [ubuntu-22]="aws-sys-ami-latest-ubuntu-22"

    [fedora-34]="aws-sys-ami-latest-fedora-34"
    [fedora-35]="aws-sys-ami-latest-fedora-35"
  )
  declare -A user_from_image=(
    [debian-10]="admin"
    [debian-11]="admin"

    [ubuntu-20]="ubuntu"
    [ubuntu-22]="ubuntu"

    [fedora-34]="fedora" # or ec2-user
    [fedora-35]="fedora" # or ec2-user
  )
  if ref-is-empty images; then
    # default to all images when no args
    images=( ${!func_from_image[@]} )
  fi
  declare region=${__region:-${AWS_REGION:-${AWS_DEFAULT_REGION:-$(aws configure get region)}}}
  declare -A image_info_from_image=()
  declare image user func image_id
  for image in ${images[@]}; do
    user=${user_from_image[$image]}
    func=${func_from_image[$image]}
    read -r image_id <<< $($func $region)
    image_info_from_image[$image]="$user $image_id"
    if true; then
      declare default_type=t2.nano
      mkdir -p lib/images/$image/
      {
        echo "# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-"
        echo
        echo "vm_image=$image_id"
        echo "vm_image_user=$user"
        echo "vm_type=${__vm_type:-$default_type}"
      } > lib/images/$image/$image.vm
    fi
  done
  declare str; str=$(ref-dump-rhs image_info_from_image)

  # optional (this is NOT a general solution)
  str=$( str=${str/(/( }; str=${str// [/\\n  [}; str=${str/ )/\\n)}; echo -e "$str" )

  echo -e "$str"
} # aws-sys-ami-latest()
