#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#shopt -s lastpipe
export PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME:-main}(): '
declare source_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

declare -r -g -x vaark_aws_vms_dir=$HOME/.vaark/aws/vms

declare __debug=${__debug:-1}

declare multiple_avail_zones=0

function die() {
  declare msg=${1:-}
  test "$msg" && msg=": $msg"
  declare progname; progname=$(basename $0) # use of basename optional
  printf "$progname: error$msg\n" 1>&2
  exit 1
} # die()

function die-dev() {
  declare msg=${1:-}
  test "$msg" && msg=": $msg"
  declare progname; progname=$(basename $0) # use of basename optional
  declare frame=${__frame:-0}
  declare lineno= funcname= src=
  read -r lineno funcname src <<< $(caller $frame)
  if ! ref-is-empty lineno; then
    frame=$(( $frame + 1 ))
    src=$(basename $src) # use of basename optional
    printf "$progname: error: %s: line %s: %s()$msg\n" \
           $src $lineno $funcname 1>&2
  else
    printf "$progname: error$msg\n" 1>&2
  fi
  exit 1
} # die-dev()

function die-dev-trace() {
  declare msg=${1:-}
  test "$msg" && msg=": $msg"
  declare progname; progname=$(basename $0) # use of basename optional
  declare frame=${__frame:-0}
  declare lineno= funcname= src=
  while read -r lineno funcname src <<< $(caller $frame); do
    ! test "$lineno" && break
    frame=$(( $frame + 1 ))
    src=$(basename $src) # use of basename optional
    printf "$progname: error: %s: line %s: %s()$msg\n" \
           $src $lineno $funcname 1>&2
    msg=
  done
  exit 1
} # die-dev-trace()

function usage() {
  declare msg=${1:-}
  printf "usage: %s %s\n" \
         $(basename $0) "$msg" 1>&2
  exit 1
} # die()

function flatten-stdin() {
  declare line d=""
  while read -r line; do
    if ! [[ $line =~ ^[[:space:]]*$ ]]; then
      printf "$d%s" "$line"
      d=" "
    fi
  done
} # flatten-stdin()

function lowercase() {
  echo "$1" | tr '[:upper:]' '[:lower:]'
} # lowercase()

function uppercase() {
  echo "$1" | tr '[:lower:]' '[:upper:]'
} # uppercase()

function duration-str() {
  declare duration=$1
  declare h; h=$((  $duration / (60  * 60) ))
  declare m; m=$(( ($duration /  60) % 60  ))
  declare s; s=$((  $duration %  60        ))

  if test $h -gt 0; then
    printf "%02dh%02dm%02ds\n" $h $m $s
  else
    printf      "%02dm%02ds\n"    $m $s
  fi
} # duration-str()

# vm-bname       $vm_1      [ $vm_2      ... ]
# vm-bname-out   $vm_1 vm_a [ $vm_2 vm_b ... ]
# vm-bname-inout       vm_a [       vm_b ... ]

function object-bname-out() {
  declare suffix=$1; shift
  test $(( $# % 2 )) -eq 0 || die-dev "Wrong number of arguments"
  while test $# -ge 2; do
    declare object=$1 object_out_ref=$2
    declare -n _object_out=$object_out_ref
    _object_out=$(basename -s $suffix $object)
    shift 2
  done
} # object-bname-out()

function object-bname() {
  declare suffix=$1; shift
  while test $# -ge 1; do
    declare object=$1 object_out=
    object-bname-out $suffix $object object_out
    echo $object_out
    shift
  done
} # object-bname()

function object-bname-inout() {
  declare suffix=$1; shift
  while test $# -ge 1; do
    declare object_ref=$1
    object-bname-out $suffix ${!object_ref} $object_ref
    shift
  done
} # object-bname-inout()

function     vm-bname()       { object-bname       .vm     "$@"; }
function  drive-bname()       { object-bname       .drive  "$@"; }
function subnet-bname()       { object-bname       .subnet "$@"; }

function     vm-bname-out()   { object-bname-out   .vm     "$@"; }
function  drive-bname-out()   { object-bname-out   .drive  "$@"; }
function subnet-bname-out()   { object-bname-out   .subnet "$@"; }

function     vm-bname-inout() { object-bname-inout .vm     "$@"; }
function  drive-bname-inout() { object-bname-inout .drive  "$@"; }
function subnet-bname-inout() { object-bname-inout .subnet "$@"; }

# bash 4.4 or later only (because of the use of 'declare -p')
function ref-type-and-rhs-out() {
  declare ref=$1 type_ref=$2 rhs_ref=$3
  declare str; str=$(declare -p $ref 2> /dev/null) || die "Var not declared: $ref"
  [[ $str =~ ^declare[[:blank:]]+(.+)[[:blank:]]+$ref=(.*)$ ]] || die "Cannot parse output of 'declare -p $ref': $str"
  declare  opts=${BASH_REMATCH[1]}
  declare _rhs_=${BASH_REMATCH[2]}
  declare _type_="-" # scalar
  [[ $opts =~ (A|a) ]] && _type_=${BASH_REMATCH[1]}
  printf -v $type_ref "$_type_"
  printf -v $rhs_ref  "$_rhs_"
} # ref-type-and-rhs-out()

# vaark formally ref-aggr-type()
function ref-type() {
  declare ref=$1
  declare type= rhs=
  ref-type-and-rhs-out $ref type rhs
  ref-is-empty type || echo "$type"
} # ref-type()

# vaark formally guest-dump-rhs()
function ref-dump-rhs() {
  declare ref=$1
  declare type= rhs=
  ref-type-and-rhs-out $ref type rhs
  ref-is-empty rhs || echo "$rhs"
} # ref-dump-rhs()

function ref-is-set() {
  declare ref=$1
  test ${!ref:-0} -ne 0
} # ref-is-set()

function ref-is-empty() {
  declare ref=$1
  ! test "${!ref:-}"
} # ref-is-empty()

function ref-is-resource-id() {
  declare ref=$1 prefix=${2:-[a-z-]+}
  [[ ${!ref:-} =~ ^$prefix-[0-9a-f]+$ ]]
} # ref-is-resource-id()

function assert-ref-is-resource-id() {
  declare ref=$1 prefix=${2:-}
  ref-is-resource-id $ref $prefix || __frame=1 die-dev "not resource-id: $ref=${!ref:-}"
} # assert-ref-is-resource-id()

function assert-ref-is-user-id() {
  declare ref=$1
  [[ ${!ref:-} =~ ^[1-9][0-9]+$ ]] || __frame=1 die-dev "not user-id: $ref=${!ref:-}"
} # assert-ref-is-user-id()

function aggr-ref-is-empty() {
  declare ref=$1
  declare str; str=$(ref-dump-rhs $ref 2> /dev/null || true)
  # str is empty if $ref was never declared
  # str is ()    if $ref was declared and empty
  test "$str" == "" || test "$str" == "()"
} # aggr-ref-is-empty()

function aws() {
  test "${__debug:-0}" -ne 0 && echo "$FUNCNAME $*" 1>&2
  test "${__msg:-}" && echo "$__msg" 1>&2
  command $FUNCNAME "$@"
} # aws()

function VBoxManage() {
  test "${__debug:-0}" -ne 0 && echo "$FUNCNAME $*" 1>&2
  test "${__msg:-}" && echo "$__msg" 1>&2
  command $FUNCNAME "$@"
} # VBoxManage()

function default-region() {
  declare region; region=${AWS_REGION:-${AWS_DEFAULT_REGION:-$(aws configure get region)}}
  echo $region
} # default-region()

function first-avail-zone-in-region() {
  declare region=${1:-}
  declare -a avail_zones; avail_zones=( $(avail-zones-in-region $region) )
  declare avail_zone=${avail_zones[0]} # so the selection is predictable
  echo $avail_zone
} # first-avail-zone-in-region()

function avail-zones-in-region() {
  declare region=${1:-$(default-region)}
  ! aggr-ref-is-empty region || die-dev "no region"
  declare -a avail_zones; avail_zones=( $(aws ec2 describe-availability-zones --region $region --output text --query "AvailabilityZones[].ZoneName") )
  ! aggr-ref-is-empty avail_zones || die-dev "no avail-zones"
  printf "%s\n" ${avail_zones[@]} | sort -V
} # avail-zones-in-region()

function default-vpc-id() { # create if missing
  declare default_vpc_id
  default_vpc_id=$(aws ec2 describe-vpcs --output text --query "Vpcs[].VpcId" --filters Name=is-default,Values=true)
  if ref-is-empty default_vpc_id; then
    default_vpc_id=$(aws ec2 create-default-vpc --output text --query "Vpc.VpcId") # and create default-security-group
  fi
  assert-ref-is-resource-id default_vpc_id vpc
  resource-id-add-name $default_vpc_id default
  echo $default_vpc_id
} # default-vpc-id()

function default-security-group-id() {
  declare default_vpc_id=$1
  assert-ref-is-resource-id default_vpc_id vpc
  declare default_security_group_id
  default_security_group_id=$(aws ec2 describe-security-groups --output text --query "SecurityGroups[].GroupId" --filters Name=group-name,Values=default Name=vpc-id,Values="$default_vpc_id")
  assert-ref-is-resource-id default_security_group_id sg
  resource-id-add-name $default_security_group_id default
  echo $default_security_group_id
} # default-security-group-id()

function dict-default-subnet-id-from-avail-zone() { # create if missing per avail-zone
  declare subnet_id_from_avail_zone_ref=$1 vpc_id=$2 region=$3
  assert-ref-is-resource-id vpc_id vpc
  declare -n _subnet_id_from_avail_zone=$subnet_id_from_avail_zone_ref
  dict.add $subnet_id_from_avail_zone_ref $(aws ec2 describe-subnets --output text --query "Subnets[].[AvailabilityZone,SubnetId]" --filter Name=vpc-id,Values="$vpc_id")

  if true; then # it seems that when you call 'aws ec2 describe-vpcs' it create both security-groups and subnets-per-avail-zone
    if ref-is-set multiple_avail_zones; then
      declare -a avail_zones; avail_zones=( $(avail-zones-in-region $region) )
    else
      declare -a avail_zones; avail_zones=( $(first-avail-zone-in-region $region) )
    fi
    if test ${#avail_zones[@]} -gt ${#_subnet_id_from_avail_zone[@]}; then
      declare avail_zone default_subnet_id
      for avail_zone in ${avail_zones[@]}; do
        if ! test -v _subnet_id_from_avail_zone[$avail_zone]; then
          default_subnet_id=$(aws ec2 create-default-subnet --output text --availability-zone $avail_zone --output text --query "Subnet.SubnetId") # one default-subnet in every avail-zone
          assert-ref-is-resource-id default_subnet_id subnet
          _subnet_id_from_avail_zone[$avail_zone]=$default_subnet_id
        fi
      done
    fi
  fi
  declare avail_zone default_subnet_id
  for avail_zone in ${!_subnet_id_from_avail_zone[@]}; do
    default_subnet_id=${_subnet_id_from_avail_zone[$avail_zone]}
    assert-ref-is-resource-id default_subnet_id subnet
    resource-id-add-name $default_subnet_id default
  done
} # dict-default-subnet-id-from-avail-zone()

function get-default-trio() {
  declare vpc_id_ref=$1 security_group_id_ref=$2 subnet_id_from_avail_zone_ref=$3 region=${4:-$(default-region)}
  declare -n _vpc_id=$vpc_id_ref _security_group_id=$security_group_id_ref _subnet_id_from_avail_zone=$subnet_id_from_avail_zone_ref
  _vpc_id=$(default-vpc-id)
  assert-ref-is-resource-id _vpc_id vpc
  _security_group_id=$(default-security-group-id $_vpc_id)
  assert-ref-is-resource-id _security_group_id sg
  dict-default-subnet-id-from-avail-zone $subnet_id_from_avail_zone_ref $_vpc_id $region

  # name default route-table
  declare rtb_id; rtb_id=$(aws ec2 describe-route-tables --output text --query "RouteTables[].RouteTableId" --filter Name=vpc-id,Values=$_vpc_id)
  assert-ref-is-resource-id rtb_id rtb
  resource-id-add-name $rtb_id default

  # name default internet-gateway
  declare igw_id; igw_id=$(aws ec2 describe-internet-gateways --output text --query "InternetGateways[].InternetGatewayId" --filter Name=attachment.vpc-id,Values=$_vpc_id)
  assert-ref-is-resource-id igw_id igw
  resource-id-add-name $igw_id default

  # name default network-acl
  declare acl_id; acl_id=$(aws ec2 describe-network-acls --output text --query "NetworkAcls[].NetworkAclId" --filter Name=vpc-id,Values=$_vpc_id)
  assert-ref-is-resource-id acl_id acl
  resource-id-add-name $acl_id default
} # get-default-trio()

function aws-sys-init-default() {
  declare region=${1:-$(default-region)}
  declare vpc_id= security_group_id=
  declare -A subnet_id_from_avail_zone=()
  get-default-trio vpc_id security_group_id subnet_id_from_avail_zone $region
  assert-ref-is-resource-id vpc_id vpc
  assert-ref-is-resource-id security_group_id sg
  declare dopt_id; dopt_id=$(aws ec2 describe-vpcs --output text --query Vpcs[].DhcpOptionsId --filter Name=vpc-id,Values=$vpc_id)
  resource-id-add-name $dopt_id default
} # aws-sys-init-default()

function tag-specs() {
  declare type=$1; shift
  declare tags= d=
  while test $# -gt 0; do
    declare key=$1 value=$2; shift 2
    tags+="$d{Key=$key,Value=$value}"
    d=","
  done
  echo "ResourceType=$type,Tags=[$tags]"
} # tag-specs()

function resource-id-add-lhs-rhs() {
  declare resource_id=$1 lhs=$2 rhs=$3
  assert-ref-is-resource-id resource_id
  declare command=command
  test ${__enable_logging:-0} -ne 0 && command=
  # using 'command aws' below prevents the use of the function aws above (to avoid logging)
  $command aws ec2 create-tags --resources $resource_id --tags Key="$lhs",Value="$rhs"
} # resource-id-add-lhs-rhs()

function resource-id-add-name() {
  declare resource_id=$1 name=$2
  assert-ref-is-resource-id resource_id
  ! ref-is-empty resource_id || die-dev "missing resource-id"
  ! ref-is-empty name        || die-dev "missing name"
  resource-id-add-lhs-rhs $resource_id Name $name
} # resource-id-add-name()

function image-id-add-user() {
  declare id=$1 user=$2
  resource-id-add-lhs-rhs $id User $user
} # image-id-add-user()

function image-id-and-user-from-name() {
  declare name=$1
  declare   id;   id=$(aws ec2 describe-tags --output text --query "Tags[].ResourceId" --filters Name=key,Values=Name Name=value,Values="$name"     Name=resource-type,Values=image)
  declare user; user=$(aws ec2 describe-tags --output text --query "Tags[].Value"      --filters Name=key,Values=User Name=resource-id,Values="$id" Name=resource-type,Values=image)
  echo $id $user
} # image-id-and-user-from-name()

function resource-ids-from-type() {
  declare resource_type=$1
  aws ec2 describe-tags --output text --query "Tags[].ResourceId" --filters Name=key,Values=Name Name=resource-type,Values="$resource_type"
} # resource-ids-from-type()

function resource-names-from-type() {
  declare resource_type=$1
  aws ec2 describe-tags --output text --query "Tags[].Value" --filters Name=key,Values=Name Name=resource-type,Values="$resource_type"
} # resource-names-from-type()

function resource-name-from-type-and-id() {
  declare resource_type=$1 resource_id=$2
  assert-ref-is-resource-id resource_id
  aws ec2 describe-tags --output text --query "Tags[].Value" --filters Name=key,Values=Name Name=resource-type,Values="$resource_type" Name=resource-id,Values="$resource_id"
} # resource-name-from-type-and-id()

function resource-id-from-type-and-name() {
  declare resource_type=$1 name=$2
  aws ec2 describe-tags --output text --query "Tags[].ResourceId" --filters Name=key,Values=Name Name=resource-type,Values="$resource_type" Name=value,Values="$name"
} # resource-id-from-type-and-name()

function dict.add() {
  declare dict_ref=$1; shift; declare -a items=( "$@" )
  declare -n _dict=$dict_ref
  test $(( ${#items[@]} % 2 )) -eq 0 || die "$dict_ref: $FUNCNAME() failed: contains an odd number of items"
  declare i
  for (( i=0; $i < ${#items[@]}; i+=2 )); do
    declare key; key=${items[ $i ]}
    declare val; val=${items[ (( $i + 1 )) ]}
    declare delim=
    if test "${_dict[$key]:-}"; then
      delim=" "
    fi
    _dict[$key]+=$delim$val
  done
} # dict.add()

function dict.add-keys() {
  declare dict_ref=$1; shift; declare -a keys=( "$@" )
  declare -n _dict=$dict_ref
  declare key
  for key in "${keys[@]}"; do
    _dict[$key]=
  done
} # dict.add-keys()

function dict.max-key-len() {
  declare dict_ref=$1
  declare -n _dict=$dict_ref
  declare max=0
  declare lhs; for lhs in ${!_dict[@]}; do test ${#lhs} -gt $max && max=${#lhs}; done
  echo $max
} # dict.max-key-len()

function dict.flatten() {
  declare dict_ref=$1
  declare -n _dict=$dict_ref
  declare max_key_len; max_key_len=$(dict.max-key-len $dict_ref)
  declare key val
  for key in "${!_dict[@]}"; do
    declare -a vals=( ${_dict[$key]} )
    ! aggr-ref-is-empty vals || die-dev "vals is empty"
    printf "%-*s %s\n" $max_key_len $key "${vals[*]}"
  done | sort -V
} # dict.flatten()

function dict-resource-id-from-type() {
  declare dict_ref=$1 name=$2
  dict.add $dict_ref $(aws ec2 describe-tags --output text --query "Tags[].[ResourceType,ResourceId]" --filters Name=key,Values=Name Name=value,Values="$name")
} # dict-resource-id-from-type()

function dict-resource-id-from-name() {
  declare dict_ref=$1 type=$2
  dict.add $dict_ref $(aws ec2 describe-tags --output text --query "Tags[].[Value,ResourceId]" --filter Name=key,Values=Name Name=resource-type,Values="$type")
} # dict-resource-id-from-name()

function            dict-vpc-id-from-name() { declare dict_ref=$1; dict-resource-id-from-name $dict_ref vpc;            }
function         dict-subnet-id-from-name() { declare dict_ref=$1; dict-resource-id-from-name $dict_ref subnet;         }
function dict-security-group-id-from-name() { declare dict_ref=$1; dict-resource-id-from-name $dict_ref security-group; }
function       dict-key-pair-id-from-name() { declare dict_ref=$1; dict-resource-id-from-name $dict_ref key-pair;       }
function       dict-instance-id-from-name() { declare dict_ref=$1; dict-resource-id-from-name $dict_ref instance;       }
function          dict-image-id-from-name() { declare dict_ref=$1; dict-resource-id-from-name $dict_ref image;          }
function         dict-volume-id-from-name() { declare dict_ref=$1; dict-resource-id-from-name $dict_ref volume;         }

function test-id-from-name() {
  declare -A            vpc_id_from_name=();            dict-vpc-id-from-name            vpc_id_from_name; declare -p            vpc_id_from_name
  declare -A         subnet_id_from_name=();         dict-subnet-id-from-name         subnet_id_from_name; declare -p         subnet_id_from_name
  declare -A security_group_id_from_name=(); dict-security-group-id-from-name security_group_id_from_name; declare -p security_group_id_from_name
  declare -A       key_pair_id_from_name=();       dict-key-pair-id-from-name       key_pair_id_from_name; declare -p       key_pair_id_from_name
  declare -A       instance_id_from_name=();       dict-instance-id-from-name       instance_id_from_name; declare -p       instance_id_from_name
  declare -A          image_id_from_name=();          dict-image-id-from-name          image_id_from_name; declare -p          image_id_from_name
  declare -A         volume_id_from_name=();         dict-volume-id-from-name         volume_id_from_name; declare -p         volume_id_from_name
} # test-id-from-name()

function avail-zone-from-instance-id() {
  declare instance_id=$1
  assert-ref-is-resource-id instance_id i
  declare -A avail_zone_from_instance_id=()
  dict.add avail_zone_from_instance_id $(aws ec2 describe-instances --output text --query "Reservations[].Instances[].[InstanceId,Placement.AvailabilityZone]")
  echo ${avail_zone_from_instance_id[$instance_id]:-}
} # avail-zone-from-instance-id()

function concur() {
  declare cmd=$1; shift; declare -a args=( "$@" )
 #printf "%s\n" ${args[@]} | xargs -L 1 -P $(( (3 * $VK_CORE_COUNT)/2 )) $cmd
  printf "%s\n" ${args[@]} | xargs -L 1 -P ${#args[@]} $cmd
} # concur()

function non-root-volume-devices-from-instance-ids() {
  declare instance_id=$1
  assert-ref-is-resource-id instance_id i
  ! test "$instance_id" && return
  # we need to omit the root drive which is always first
  declare -a tmp; tmp=( $(aws ec2 describe-instances --instance-ids $instance_id --output text --query "Reservations[].Instances[].BlockDeviceMappings[].DeviceName") )
  printf "%s\n" ${tmp[@]:1}
} # non-root-volume-devices-from-instance-ids()

function non-root-volume-ids-from-instance-id() {
  declare instance_id=$1
  assert-ref-is-resource-id instance_id i
  ! test "$instance_id" && return
  # we need to omit the root drive which is always first
  declare -a tmp; tmp=( $(aws ec2 describe-instances --instance-ids $instance_id --output text --query "Reservations[].Instances[].BlockDeviceMappings[].Ebs.VolumeId") )
  printf "%s\n" ${tmp[@]:1}
} # non-root-volume-ids-from-instance-id()

function root-volume-id-from-instance-id() {
  declare instance_id=$1
  assert-ref-is-resource-id instance_id i
  aws ec2 describe-instances --instance-ids $instance_id  --output text --query "Reservations[].Instances[].BlockDeviceMappings[0].Ebs.VolumeId"
} # root-volume-id-from-instance-id()

function attached-non-root-volume-ids-from-instance-id() {
  declare instance_id=$1
  assert-ref-is-resource-id instance_id i
  # we need to omit the root drive which is always first
  declare -a tmp; tmp=( $(aws ec2 describe-instances --output text --query "Reservations[].Instances[].BlockDeviceMappings[].Ebs.VolumeId" --filters Name=instance-id,Values="$instance_id") )
  printf "%s\n" ${tmp[@]:1}
} # attached-non-root-volume-ids-from-instance-id()

function instance-id-exists-pcmd() {
  declare instance_id=$1
  assert-ref-is-resource-id instance_id i
  # returns empty when instance_id is empty
  declare id; id=$(aws ec2 describe-instances --output text --query "Reservations[].Instances[].InstanceId" --filters Name=instance-id,Values="$instance_id")
  test $id && echo true || echo false
} # instance-id-exists-pcmd()

function image-id-exists-pcmd() {
  declare image_id=$1
  assert-ref-is-resource-id image_id image
  # returns empty when image_id is empty
  declare id; id=$(aws ec2 describe-images --output text --query "Images[].ImageId" --filters Name=image-id,Values="$image_id")
  test $id && echo true || echo false
} # image-id-exists-pcmd()

function volume-id-exists-pcmd() {
  declare volume_id=$1
  assert-ref-is-resource-id volume_id vol
  # returns empty when volume_id is empty
  declare id; id=$(aws ec2 describe-volumes --output text --query "Volumes[].VolumeId" --filters Name=volume-id,Values="$volume_id")
  test $id && echo true || echo false
} # volume-id-exists-pcmd()

function configure-file() {
  declare vars_dict_ref=$1 file=$2
  declare -n _vars_dict=$vars_dict_ref
  declare lhs rhs
  for lhs in ${!_vars_dict[@]}; do
    rhs=${_vars_dict[$lhs]}
    sed -i~ -E -e "s|@@$lhs@@|$rhs|g" "$file"
    rm -f "$file"~
  done
} # configure-file()

function ssh-config-dir() {
  declare vm=$1
  vm-bname-inout vm
  echo $vaark_aws_vms_dir/$vm/ssh
} # ssh-config-dir()

function ssh-config-update() {
  declare vm=$1
  vm-ssh-config-generate-hostname-decl $vm
} # ssh-config-update()

function socket-is-listening() {
  declare port=$1 ipv4_addr=${2:-127.0.0.1}
  # be careful here. netcat has different options on debian vs redhat
# nc -n -z $ipv4_addr $port 1> /dev/null 2>&1 # netcat
  nc -n -z $ipv4_addr $port 1>&2
} # socket-is-listening()

function public-ip-addr-from-instance-id() {
  declare instance_id=$1
  assert-ref-is-resource-id instance_id i
  declare public_ipv4_addr;  public_ipv4_addr=$(aws ec2 describe-instances --instance-ids $instance_id --output text --query "Reservations[].Instances[].PublicIpAddress")
  ! ref-is-empty public_ipv4_addr  || die "missing public-ipv4-addr"
  echo $public_ipv4_addr
}

# maybe we should consider using this instead of creating hostname-decl.txt
# https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/Connect-using-EC2-Instance-Connect.html

function vm-ssh-config-hostname-decl-file() { declare vm=$1; echo $HOME/.vaark/aws/vms/$(basename $vm)/ssh/hostname-decl.txt; }

function vm-ssh-config-generate-hostname-decl() {
  declare vm=$1
  vm-bname-inout vm
  declare instance_id;       instance_id=$(resource-id-from-type-and-name instance $vm)
  ! ref-is-empty instance_id  || die "no such vm $vm"
  assert-ref-is-resource-id instance_id i
  declare public_ipv4_addr;   public_ipv4_addr=$(public-ip-addr-from-instance-id $instance_id)
  declare hostname_decl_file; hostname_decl_file=$(vm-ssh-config-hostname-decl-file $vm)
  echo "HostName $public_ipv4_addr" > $hostname_decl_file
  chmod 0640 $hostname_decl_file
  # declare private_ipv4_addr; private_ipv4_addr=$(aws ec2 describe-instances --instance-ids $instance_id --output text --query "Reservations[].Instances[].PrivateIpAddress")
  # ! ref-is-empty private_ipv4_addr || die "missing private-ipv4-addr"
} # vm-ssh-config-generate-hostname-decl()

function run-top-level-cmd() {
  if ! declare -F $1 > /dev/null; then
    echo "$(basename "${BASH_SOURCE[1]}"): error: Cannot find top-level function named $1" 1>&2
    exit 1
  fi
  "$@"
} # run-top-level-cmd()
