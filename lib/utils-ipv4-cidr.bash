#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#shopt -s lastpipe
export PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME:-main}(): '
declare source_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

source "$VAARK_AWS_LIB_DIR/utils.bash"

function hash-from-str() { #_vars_="" usage1 str "$@"
  declare str=$1
  ! ref-is-empty str || die-dev "Only arg to hash-from-str() may not be empty string."
  declare val junk
  read -r val junk <<< $(echo "$str" | cksum) # only need first output word from cksum
  echo $val
} # hash-from-str()

function ipv4-addr-split() {
  declare ipv4_addr=$1
  declare -a parts=()
  declare regex="^([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)$"
  [[ $ipv4_addr =~ $regex ]] \
    || die-dev "Not well formed ipv4-addr: $ipv4_addr"
  parts=( ${BASH_REMATCH[1]} ${BASH_REMATCH[2]} ${BASH_REMATCH[3]} ${BASH_REMATCH[4]} )
  declare octet
  for octet in ${parts[@]}; do
    if test $octet -lt 0 || test $octet -gt 255; then
      die-dev "Not well formed ipv4-addr: $ipv4_addr"
    fi
  done
  echo ${parts[@]}
} # ipv4-addr-split()

function ipv4-addr-values() {
  declare -a ipv4_addrs=( $@ )
  declare bs=256 # ipv4-constant
  declare -a results=()
  declare ipv4_addr
  for ipv4_addr in ${ipv4_addrs[@]}; do
    declare -a cols; cols=( $(ipv4-addr-split $ipv4_addr) )
    declare total=0
    total=$(( $total + (${cols[0]} * ($bs**3)) ))
    total=$(( $total + (${cols[1]} * ($bs**2)) ))
    total=$(( $total + (${cols[2]} * ($bs**1)) ))
    total=$(( $total + (${cols[3]} * ($bs**0)) ))
    results+=( $total )
  done
  printf "%s\n" ${results[@]}
} # ipv4-addr-values()

function ipv4-addr-mod() {
  declare ipv4_addr=$1 n=$2
  declare total; total=$(ipv4-addr-values $ipv4_addr)
  echo $(( $total % $n ))
} # ipv4-addr-mod()

function ipv4-addr-add() {
  declare ipv4_addr=$1 n=$2
  declare bs=256 # ipv4-constant
  declare -a cols; cols=( $(ipv4-addr-split $ipv4_addr) )
  declare i
  for (( i=$(( ${#cols[@]} - 1 )); $i >= 0; i-- )); do
    cols[$i]=$(( ${cols[$i]} + $n ))
    n=0
    declare carry; carry=$(( ${cols[$i]} / $bs ))
    cols[$i]=$(( ${cols[$i]} % $bs ))
    if test $i -gt 0; then
      cols[$i - 1]=$(( ${cols[$i - 1]} + $carry ))
    fi
  done
  test ${cols[0]} -lt $bs || die-dev "Overflow error: ipv4-addr-add $ipv4_addr $n"
  (IFS="."; printf '%s\n' "${cols[*]}")
} # ipv4-addr-add()

function ipv4-cidr-bisect() { #_vars_="" usage3 str lhs-ref rhs-ref "$@"
  declare ipv4_cidr=$1 lhs_ref=$2 rhs_ref=$3
  declare -a parts=()
  declare regex="^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/([0-9]+)$"
  [[ $ipv4_cidr =~ $regex ]] \
    || die-dev "Not well formed ipv4-cidr: $ipv4_cidr"
  parts=( ${BASH_REMATCH[1]} ${BASH_REMATCH[2]} )
  ipv4-addr-split ${parts[0]} > /dev/null # this will range check each octet
  printf -v $lhs_ref "%s" ${parts[0]}
  printf -v $rhs_ref "%s" ${parts[1]}
} # ipv4-cidr-bisect()

function ipv4-cidr-validate() {
  declare ipv4_cidr=$1
  declare mask_max=32 # ipv4-constant
  declare ipv4_addr= mask=
  ipv4-cidr-bisect $ipv4_cidr ipv4_addr mask
  declare mod; mod=$(ipv4-addr-mod $ipv4_addr $(( 2**($mask_max - $mask) )) )
  test $mod -eq 0 || die "invalid cidr: $ipv4_cidr"
} # ipv4-cidr-validate()

function ipv4-cidr-range() {
  declare ipv4_cidr=$1
  declare mask_max=32 # ipv4-constant
  declare ipv4_addr= mask=
  ipv4-cidr-bisect $ipv4_cidr ipv4_addr mask
  declare first=$ipv4_addr
  declare last; last=$(ipv4-addr-add $first $(( 2**($mask_max - $mask) - 1 )) )
  echo $first $last
} # ipv4-cidr-range()

function ipv4-cidr-overlap-pcmd() {
  test $# -ge 2 || die-dev "$FUNCNAME() requires two or more args"
  declare cidr_a= cidr_b=$1; shift # cidr_a is intentionally empty
  declare pcmd=false
  while test $# -gt 0; do
    cidr_a=$cidr_b; cidr_b=$1; shift

    declare -a value_a; value_a=( $(ipv4-addr-values $(ipv4-cidr-range $cidr_a)) )
    declare -a value_b; value_b=( $(ipv4-addr-values $(ipv4-cidr-range $cidr_b)) )

    # not sure about this
    # https://stackoverflow.com/questions/17206679/check-if-two-cidr-addresses-intersect
    # also
    # https://unix.stackexchange.com/questions/430588/check-overlapped-subnets
    if test ${value_a[0]} -le ${value_b[1]} && \
       test ${value_b[0]} -le ${value_a[1]}; then
      pcmd=true
      break
    fi
  done
  echo $pcmd
} # ipv4-cidr-overlap-pcmd()

function subnet-generate-ipv4-cidr() {
  declare ntwk_ipv4_cidr=$1 sbnt_mask=$2 val=$3
  declare mask_max=32 # ipv4-constant

  declare ntwk_ipv4_addr= ntwk_mask=
  ipv4-cidr-bisect $ntwk_ipv4_cidr ntwk_ipv4_addr ntwk_mask

  declare lhs_n; lhs_n=$(( $sbnt_mask - $ntwk_mask ))
  declare rhs_n; rhs_n=$(( $mask_max - $sbnt_mask ))
  declare lhs; lhs=$(( $val % (2**$lhs_n) ))
  declare rhs; rhs=$(( $val % (2**$rhs_n) ))
  lhs=$(( $lhs << $rhs_n ))
  rhs=$(( $rhs * (2**$rhs_n) ))
  declare offset; offset=$(( $lhs | $rhs ))

  declare ipv4_addr; ipv4_addr=$(ipv4-addr-add $ntwk_ipv4_addr $offset)
  declare ipv4_cidr=$ipv4_addr/$sbnt_mask
  echo $ipv4_cidr
} # subnet-generate-ipv4-cidr()

function subnet-ipv4-cidr-from-name() {
  declare subnet=$1 ntwk_ipv4_cidr=$2 sbnt_mask=$3
  declare ntwk_ipv4_addr= ntwk_mask=
  ipv4-cidr-bisect $ntwk_ipv4_cidr ntwk_ipv4_addr ntwk_mask
  declare num_sbnts; num_sbnts=$(( 2**($sbnt_mask - $ntwk_mask) ))

  declare str=$subnet
  declare max_collision_retries=5
  test ${retry_count:-0} -le $max_collision_retries \
    || die "$subnet.subnet: Unable to find (by hashing) an available IPV4-CIDR (already retried $max_collision_retries times)"
  if ref-is-set retry_count; then
    str=$str~$retry_count~
  fi
  declare val; val=$(hash-from-str "$str")

  val=$(( $val % $num_sbnts ))
  declare sbnt_cidr; sbnt_cidr=$(subnet-generate-ipv4-cidr \
    $ntwk_ipv4_cidr \
    $sbnt_mask \
    $val)
  ipv4-cidr-validate $sbnt_cidr

  declare -a ipv4_cidrs=( $sbnt_cidr )
  ipv4_cidrs+=( $(aws ec2 describe-subnets --output text --query "Subnets[].CidrBlock") )
  ipv4_cidrs=( $(printf "%s\n" ${ipv4_cidrs[@]} | sort -V) )

  declare overlap_pcmd; overlap_pcmd=$(ipv4-cidr-overlap-pcmd ${ipv4_cidrs[@]})
  if $overlap_pcmd; then
    retry_count=$(( ${retry_count:-0} + 1 )) ipv4_cidr=$(subnet-ipv4-cidr-from-name $subnet $ntwk_ipv4_cidr $sbnt_mask) # recursive
  fi
  echo $sbnt_cidr
} # subnet-ipv4-cidr-from-name()
