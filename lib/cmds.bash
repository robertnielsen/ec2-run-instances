# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
declare source_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

source "$VAARK_AWS_LIB_DIR/utils-ipv4-cidr.bash"

function aws-drive-create() {
  declare -a desc_files=( "$@" )
  test ${#desc_files[@]} -eq 0 && return

  if test ${#desc_files[@]} -gt 1; then concur $VAARK_AWS_BIN_DIR/vrk-aws-drive-create ${desc_files[@]}; return; fi # indirectly recursive
  declare desc_file=${desc_files[0]}
  desc_file=$(drive-desc-file-from-dir $desc_file)
  declare drive=; drive-bname-out $desc_file drive

  declare default_drive_size=10
  declare default_region; default_region=$(default-region)
  declare default_subnet_avail_zone; default_subnet_avail_zone=$(first-avail-zone-in-region $default_region)

  source "$VAARK_AWS_LIB_DIR/object-schema/object-schema-drive-init.bash"

  drive_size=$default_drive_size
  drive_region=$default_region
  drive_avail_zone=$default_subnet_avail_zone

  if test -e "$desc_file"; then
    source   "$desc_file"
  fi

  declare volume_id
  volume_id=$(resource-id-from-type-and-name volume $drive)
  if ref-is-empty volume_id; then
    echo "# $drive.drive: creating" 1>&2
    volume_id=$(aws ec2 create-volume --region $drive_region --availability-zone $drive_avail_zone --size $drive_size --output text --query "VolumeId" \
                    --tag-specifications $(tag-specs volume Name $drive) ) # vol-<>
    assert-ref-is-resource-id volume_id vol
    ref-is-set __async && return
    declare t=$SECONDS
    __msg="# $drive.drive: volume-available: wait..." aws ec2 wait volume-available --volume-ids $volume_id
    echo  "# $drive.drive: volume-available: wait...done:" $(duration-str $(( $SECONDS - $t )) ) 1>&2
  else
    echo "# $drive.drive aleady exists" 1>&2
  fi
} # aws-drive-create()

function aws-drive-destroy() {
  declare -a drives=( "$@" )
  test ${#drives[@]} -eq 0 && return

  if test ${#drives[@]} -gt 1; then concur $VAARK_AWS_BIN_DIR/vrk-aws-drive-destroy ${drives[@]}; return; fi # indirectly recursive
  declare drive=${drives[0]}
  drive-bname-inout drive
  declare volume_id; volume_id=$(resource-id-from-type-and-name volume $drive)
  if ! ref-is-empty volume_id; then
    declare volume_id_exists_pcmd; volume_id_exists_pcmd=$(volume-id-exists-pcmd $volume_id)
    if $volume_id_exists_pcmd; then
      echo "# $drive.drive: destroying" 1>&2
      aws ec2 delete-volume --volume-id $volume_id
      ref-is-set __async && return
      declare t=$SECONDS
      __msg="# $drive.drive: volume-deleted: wait..." aws ec2 wait volume-deleted --volume-ids $volume_id
      echo  "# $drive.drive: volume-deleted: wait...done:" $(duration-str $(( $SECONDS - $t )) ) 1>&2
    fi
    aws ec2 delete-tags --resources $volume_id
  fi
} # aws-drive-destroy()

function ssh-config-init() {
  declare vm=$1 user=$2 key_file=$3 instance_id=$4
  assert-ref-is-resource-id instance_id i
  declare hostname_decl_file; hostname_decl_file=$(vm-ssh-config-hostname-decl-file $(basename $vm))

  declare -A vars_dict=(
    [Host]=$(basename $vm)
    [instance-id]=$instance_id
    [User]=$user
    [HostName]="${hostname_decl_file/#$HOME\//\~/}"
    [Port]=22
    [IdentityFile]="${key_file/#$HOME\//\~/}"
  )
  declare dir; dir=$(ssh-config-dir $vm)
  cp $VAARK_AWS_LIB_DIR/templates/ssh-config.in $dir/config
  configure-file vars_dict  $dir/config
  chmod 0640 $dir/config
  ssh-config-update $vm
} # ssh-config-init()

function obj-desc-file-from-dir() {
  declare obj=$1 desc_file=$2
  if test -d $desc_file; then
    declare name; name=$(basename $desc_file)
    if test -e $desc_file/$name.$obj; then
      desc_file=$desc_file/$name.$obj
    fi
  fi
  echo $desc_file
} # obj-desc-file-from-dir()

function vm-desc-file-from-dir() {
  obj-desc-file-from-dir vm "$@"
} # vm-desc-file-from-dir()

function drive-desc-file-from-dir() {
  obj-desc-file-from-dir drive "$@"
} # drive-desc-file-from-dir()

function subnet-desc-file-from-dir() {
  obj-desc-file-from-dir subnet "$@"
} # subnet-desc-file-from-dir()

function aws-vm-build() {
  declare desc_file=$1
  desc_file=$(vm-desc-file-from-dir $desc_file)
  declare image; image=$(source "$VAARK_AWS_LIB_DIR/object-schema/object-schema-vm-init.bash"; source "$desc_file"; echo $vm_image)
  aws-vm-clone $image $desc_file
} # aws-vm-build()

function ssh-key-from-proto() {
  declare proto=$1
  declare proto_ssh_key=
  declare proto_desc_file=$HOME/.vaark/vbox/vms/$proto/vaark/$proto.vm
  if test -e $proto_desc_file; then
    declare -a proto_ssh_keys; proto_ssh_keys=( $(source "$proto_desc_file"; echo ${vm_basevm_ssh_keys[@]}) )
    for proto_ssh_key in ${proto_ssh_keys[@]}; do
      test -e $proto_ssh_key && break
      proto_ssh_key=
    done
    echo $proto_ssh_key
  fi
} # ssh-key-from-proto()

function avail-zone-from-subnet-id() {
  declare subnet_id=$1
  assert-ref-is-resource-id subnet_id subnet
  aws ec2 describe-subnets --output text --query "Subnets[].AvailabilityZone" --filter Name=subnet-id,Values="$subnet_id"
} # avail-zone-from-subnet-id()

function vpc-id-from-subnet-id() {
  declare subnet_id=$1
  assert-ref-is-resource-id subnet_id subnet
  aws ec2 describe-subnets --subnet-ids $subnet_id --output text --query "Subnets[].VpcId"
} # vpc-id-from-subnet-id()

function vpc-from-subnet() {
  declare subnet=$1
  declare -A vpc_id_from_subnet=(); dict.add vpc_id_from_subnet $(aws ec2 describe-subnets --output text --query "Subnets[].[Tags[].Value,[VpcId]]" --filter Name=tag:Name,Values="*")
  declare vpc_id; vpc_id=${vpc_id_from_subnet[$subnet]:-}
  ref-is-empty vpc_id && return
  assert-ref-is-resource-id vpc_id vpc

  declare -A vpc_from_vpc_id=(); dict.add vpc_from_vpc_id       $(aws ec2 describe-vpcs    --output text --query "Vpcs[].[[VpcId],Tags[].Value]"    --filter Name=tag:Name,Values="*")
  declare vpc; vpc={vpc_from_vpc_id[$vpc_id]:-}
  ref-is-empty vpc && return
  echo $vpc
} # vpc-from-subnet()

# may return 0, 1, or many results; it should only return 0 or 1 results
function vpc-id-from-name() {
  declare name=$1
  aws ec2 describe-vpcs --output text --query "Vpcs[].VpcId" --filter Name=tag:Name,Values="$name"
} # vpc-id-from-name()

# may return 0, 1, or many results; it should only return 0 or 1 results
function security-group-id-from-name() {
  declare name=$1
  aws ec2 describe-security-groups --output text --query "SecurityGroups[].GroupId" --filter Name=tag:Name,Values="$name"
} # security-group-id-from-name()

# may return 0, 1, or many results; it should only return 0 or 1 results
function subnet-id-from-name() {
  declare name=$1
  aws ec2 describe-subnets --output text --query "Subnets[].SubnetId" --filter Name=tag:Name,Values="$name"
} # subnet-id-from-name()

function subnet-id-from-vpc-id-and-avail-zone() {
  declare vpc_id=$1 avail_zone=$1
  assert-ref-is-resource-id vpc_id vpc
  aws ec2 describe-subnets --output text --query "Subnets[].SubnetId" --filter Name=vpc-id,Values=$vpc_id Name=availability-zone,Values=$avail_zone
} # subnet-id-from-vpc-id-and-avail-zone()

function subnets-from-avail-zone() {
  declare avail_zone=$1
  aws ec2 describe-subnets --output text --query "Subnets[].Tags[].Value" --filter Name=tag-key,Values=Name Name=availability-zone,Values=$avail_zone
} # subnets-from-avail-zone()

function key-pair-id-from-name() {
  declare name=$1
  aws ec2 describe-key-pairs --output text --query "KeyPairs[].KeyPairId" --filter Name=key-name,Values="$name"
} # key-pair-id-from-name()

function aws-vpc-create() {
  declare vpc=$1 cidr_block=$2
  {
    declare vpc_id; vpc_id=$(vpc-id-from-name $vpc)
    if ref-is-empty vpc_id; then
      vpc_id=$(aws ec2 create-vpc --cidr-block $cidr_block --output text --query "Vpc.VpcId" \
                   --tag-specifications $(tag-specs vpc Name $vpc) ) # vpc-<>
      assert-ref-is-resource-id vpc_id vpc

      # name default security-group created by "aws ec2 create-vpc"
      declare security_group_id; security_group_id=$(aws ec2 describe-security-groups --output text --query "SecurityGroups[].GroupId" --filter Name=vpc-id,Values="$vpc_id")
      assert-ref-is-resource-id security_group_id sg
      resource-id-add-name $security_group_id $vpc-sg-default

      # name default main route-table created by "aws ec2 create-vpc"
      declare main_rtb_id; main_rtb_id=$(aws ec2 describe-route-tables --output text --query "RouteTables[].RouteTableId" --filter Name=vpc-id,Values="$vpc_id" Name=association.main,Values=true)
      assert-ref-is-resource-id main_rtb_id rtb
      resource-id-add-name $main_rtb_id $vpc-rtb-main

      # internet-gateway is set when creating the vpc
      declare -A igw_id_from_name=(); dict.add igw_id_from_name $(aws ec2 describe-internet-gateways --output text --query "InternetGateways[].[Tags[].Value,[InternetGatewayId]]")
      declare igw_id; igw_id=${igw_id_from_name[$vpc]:-}
      if ref-is-empty igw_id; then
        igw_id=$(aws ec2 create-internet-gateway --output text --query "InternetGateway.InternetGatewayId" \
                     --tag-specifications $(tag-specs internet-gateway Name $vpc-igw) ) # igw-<>
        assert-ref-is-resource-id igw_id igw
      fi
      assert-ref-is-resource-id igw_id igw
      aws ec2 attach-internet-gateway --vpc-id $vpc_id --internet-gateway-id $igw_id
      # route
      declare rtn; rtn=$(aws ec2 create-route --route-table-id $main_rtb_id --destination-cidr-block 0.0.0.0/0 --gateway-id $igw_id --output text --query "Return")
      test $(lowercase $rtn) == true || die-dev "aws ec2 create-route"
    else
      echo "# vpc already exists with same name $vpc" 1>&2
    fi
    aws ec2 modify-vpc-attribute --vpc-id $vpc_id --enable-dns-support   Value=true
    aws ec2 modify-vpc-attribute --vpc-id $vpc_id --enable-dns-hostnames Value=true
  } 1>&2
  echo $vpc_id
} # aws-vpc-create()

# make sure all the defaults exist and are tag-named
function aws-sys-init() {
  declare region=${1:-$(default-region)}

  aws-sys-init-default

  # where should we put these?
  declare default_vpc=vpc-0

  declare vpc_cidr_block=10.10.0.0/16 # fixfix
  declare vpc_subnet_mask=26 # fixfix

  declare vpc_id; vpc_id=$(aws-vpc-create $default_vpc $vpc_cidr_block)
  assert-ref-is-resource-id vpc_id vpc

  declare -a avail_zones; avail_zones=( $(avail-zones-in-region $region) )
  declare avail_zone
  for (( i=0; $i < ${#avail_zones[@]}; i++ )); do
    avail_zone=${avail_zones[$i]}
    declare -A subnets_set=(); dict.add-keys subnets_set $(subnets-from-avail-zone $avail_zone)
    declare default_subnet_base=$default_vpc-subnet-$i
    declare subnet=$default_subnet_base-$avail_zone
    declare subnet_vpc=$default_vpc
    if ! test -v subnets_set[$subnet]; then
      declare subnet_cidr_block; subnet_cidr_block=$(subnet-ipv4-cidr-from-name $subnet $vpc_cidr_block $vpc_subnet_mask)
      declare subnet_id=; aws-subnet-create-out $subnet $subnet_vpc $subnet_cidr_block $avail_zone subnet_id
      assert-ref-is-resource-id subnet_id subnet
    fi
  done

  function resource-id-add-vpc-name() {
    declare resource_ids_from_vpc_id_ref=$1 resource_type=$2 suffix=${3:-}
    declare -n _resource_ids_from_vpc_id=$resource_ids_from_vpc_id_ref
    declare vcp_id name resource_id
    for vpc_id in ${!_resource_ids_from_vpc_id[@]}; do
      for resource_id in ${_resource_ids_from_vpc_id[$vpc_id]}; do
        assert-ref-is-resource-id resource_id $resource_type
        name=$(aws ec2 describe-vpcs --vpc-id $vpc_id --output text --query "Vpcs[].Tags[].Value" --filter Name=tag-key,Values=Name)
        if ! ref-is-empty name; then
          if test "$name" == default; then
            resource-id-add-name $resource_id $name
          else
            resource-id-add-name $resource_id $name-${suffix:-$resource_type}
          fi
        else
          echo "unable to name resource $resource_id since its vpc $vpc_id is not named" 1>&2
        fi
      done
    done
  }

  # tag the default internet-gateways with the same name as the vpc (either 'default' or 'vpc-<N>' or ...)
  declare -A internet_gateway_id_from_vpc_id=(); dict.add internet_gateway_id_from_vpc_id $(aws ec2 describe-internet-gateways --output text --query "InternetGateways[].[Attachments[].VpcId,[InternetGatewayId]]")
  resource-id-add-vpc-name internet_gateway_id_from_vpc_id igw

  # tag the default network-acls with the same name as the vpc (either 'default' or 'vpc-<N>' or ...)
  declare -A ntwk_acl_id_from_vpc_id=(); dict.add ntwk_acl_id_from_vpc_id $(aws ec2 describe-network-acls --output text --query "NetworkAcls[].[VpcId,NetworkAclId]")
  resource-id-add-vpc-name ntwk_acl_id_from_vpc_id acl acl-default
} # aws-sys-init()

function aws-sys-show() {
  declare -a resources
  echo "# vpcs"
  resources=( $(command aws ec2 describe-vpcs              --output text --query "Vpcs[].[Tags[].Value,[VpcId]]") )
  aggr-ref-is-empty resources || printf "  % -25s %s\n" ${resources[@]} | sort -V
  echo "# security-groups"
  resources=( $(command aws ec2 describe-security-groups   --output text --query "SecurityGroups[].[Tags[].Value,[GroupId]]") )
  aggr-ref-is-empty resources || printf "  % -25s %s\n" ${resources[@]} | sort -V
  echo "# route-tables"
  resources=( $(command aws ec2 describe-route-tables      --output text --query "RouteTables[].[Tags[].Value,[RouteTableId]][][]" --filter Name=tag-key,Values=Name) )
  aggr-ref-is-empty resources || printf "  % -25s %s\n" ${resources[@]} | sort -V
  echo "# internet-gateways"
  resources=( $(command aws ec2 describe-internet-gateways --output text --query "InternetGateways[].[Tags[].Value,[InternetGatewayId]]") )
  aggr-ref-is-empty resources || printf "  % -25s %s\n" ${resources[@]} | sort -V
  echo "# network-acls"
  resources=( $(command aws ec2 describe-network-acls      --output text --query "NetworkAcls[].[Tags[].Value,[NetworkAclId]]") )
  aggr-ref-is-empty resources || printf "  % -25s %s\n" ${resources[@]} | sort -V
  echo "# subnets"
  resources=( $(command aws ec2 describe-subnets           --output text --query "Subnets[].[Tags[].Value,[SubnetId]]") )
  aggr-ref-is-empty resources || printf "  % -25s %s\n" ${resources[@]} | sort -V
  echo "# instances"
  resources=( $(command aws ec2 describe-instances         --output text --query "Reservations[].Instances[].[Tags[].Value,[InstanceId]]" --filter Name=tag-key,Values=Name) )
  aggr-ref-is-empty resources || printf "  % -25s %s\n" ${resources[@]} | sort -V
} # aws-sys-show()

function aws-sys-graph() {
  declare command=
  declare -a cmds=(
    "aws ec2 describe-vpcs              --output text --query Vpcs[].[VpcId,DhcpOptionsId]"
   #"aws ec2 describe-vpcs              --output text --query Vpcs[].[[VpcId],CidrBlockAssociationSet[].AssociationId]"
    "aws ec2 describe-security-groups   --output text --query SecurityGroups[].[GroupId,VpcId]"
   #"aws ec2 describe-route-tables      --output text --query RouteTables[].Associations[].[RouteTableId,RouteTableAssociationId]"
    "aws ec2 describe-route-tables      --output text --query RouteTables[].[VpcId,RouteTableId]"

    "aws ec2 describe-route-tables      --output text --query RouteTables[].[Routes[1].GatewayId,RouteTableId]"

    "aws ec2 describe-internet-gateways --output text --query InternetGateways[].[[InternetGatewayId],Attachments[].VpcId]"
    "aws ec2 describe-network-acls      --output text --query NetworkAcls[].Associations[].[NetworkAclId,SubnetId]"
    "aws ec2 describe-network-acls      --output text --query NetworkAcls[].[NetworkAclId,VpcId]"
    "aws ec2 describe-subnets           --output text --query Subnets[].[SubnetId,VpcId]"
   #"aws ec2 describe-subnets           --output text --query Subnets[].[SubnetId,AvailabilityZone]"
  )
  declare -a name_cmds=(
    "aws ec2 describe-dhcp-options      --output text --query DhcpOptions[].[DhcpOptionsId,Tags[].Value] --filter Name=tag-key,Values=Name"
    "aws ec2 describe-vpcs              --output text --query Vpcs[].[VpcId,Tags[].Value] --filter Name=tag-key,Values=Name"
   #"aws ec2 describe-vpcs              --output text --query Vpcs[].[[VpcId],CidrBlockAssociationSet[].AssociationId]"
    "aws ec2 describe-security-groups   --output text --query SecurityGroups[].[GroupId,Tags[].Value] --filter Name=tag-key,Values=Name"
   #"aws ec2 describe-route-tables      --output text --query RouteTables[].Associations[].[RouteTableId,RouteTableAssociationId]"
    "aws ec2 describe-route-tables      --output text --query RouteTables[].[RouteTableId,Tags[].Value] --filter Name=tag-key,Values=Name"

    "aws ec2 describe-internet-gateways --output text --query InternetGateways[].[[InternetGatewayId],Tags[].Value] --filter Name=tag-key,Values=Name"
    "aws ec2 describe-network-acls      --output text --query NetworkAcls[].[NetworkAclId,Tags[].Value] --filter Name=tag-key,Values=Name"

    "aws ec2 describe-subnets           --output text --query Subnets[].[SubnetId,Tags[].Value] --filter Name=tag-key,Values=Name"
   #"aws ec2 describe-subnets           --output text --query Subnets[].[SubnetId,AvailabilityZone]"
  )
  declare -A edges=()
  declare cmd
  for cmd in "${cmds[@]}"; do
    dict.add edges $($command $cmd)
  done

  declare -A name_from_resource_id=()
  declare cmd
  for cmd in "${name_cmds[@]}"; do
    dict.add name_from_resource_id $($command $cmd)
  done
  # declare -p edges name_from_resource_id 1>&2
  {
    printf 'digraph {\n'
    printf '  graph [ rankdir = LR ];\n'
    printf '  node  [ shape = rect, style = rounded ];\n'

    printf "\n"
    declare resource_id name resource_abbrv
    for resource_id in ${!name_from_resource_id[@]}; do
      name=${name_from_resource_id[$resource_id]}
      resource_abbrv=$resource_id
      if false; then
        [[ $resource_id =~ ^([a-z]+[a-z0-9-]+)-[0-9a-f]+$ ]] && resource_abbrv="${BASH_REMATCH[1]}-<>"
      fi
      printf "  \"$resource_id\" [ label = \"$name\\\n$resource_abbrv\" ];\n"
    done | sort -V

    printf "\n"
    declare lhs; declare -a rhss
    for lhs in ${!edges[@]}; do
      rhss=( ${edges[$lhs]} )
      declare rhs
      for rhs in ${rhss[@]}; do
        printf '  "%s" -> "%s";\n' $lhs $rhs
      done
    done | sort -V

    printf '}\n'
  }
} # aws-sys-graph()

function aws-security-group-create() {
  declare security_group=$1 vpc=$2
  {
    declare vpc_id; vpc_id=$(vpc-id-from-name $vpc)
    ! ref-is-empty vpc_id || die "No such vpc $vpc"
    declare security_group_id; security_group_id=$(security-group-id-from-name $security_group)
    if ref-is-empty security_group_id; then
      security_group_id=$(aws ec2 create-security-group --group-name $security_group --description "security group for SSH access" --vpc-id $vpc_id --output text --query "GroupId" \
                              --tag-specifications $(tag-specs security-group Name $security_group) ) # sg-<>
    else
      echo "# security-group already exists with same name $security_group" 1>&2
    fi
  } 1>&2
  assert-ref-is-resource-id security_group_id sg
  echo $security_group_id
} # aws-security-group-create()

function aws-security-group-add-ingress() {
  declare security_group=$1 port=$2 protocol=$3 cidr=$4
  {
    declare security_group_id; security_group_id=$(security-group-id-from-name $security_group)
    if ! ref-is-empty security_group_id; then
      assert-ref-is-resource-id security_group_id sg
      declare sgr_id; sgr_id=$(aws ec2 authorize-security-group-ingress --group-id $security_group_id --port $port --protocol $protocol --cidr $cidr --output text --query "SecurityGroupRules[0].SecurityGroupRuleId") # sgr-<>
      assert-ref-is-resource-id sgr_id sgr
    fi
  } 1>&2
} # aws-security-group-add-ingress()

function aws-subnet-create() {
  declare subnet=$1 vpc=$2 subnet_cidr_block=$3 avail_zone=$4
  declare subnet_id=; aws-subnet-create-out $subnet $vpc $subnet_cidr_block $avail_zone subnet_id
} # aws-subnet-create()

function aws-subnet-create-out() {
  declare subnet=$1 vpc=$2 subnet_cidr_block=$3 avail_zone=$4 subnet_id_ref=$5
  declare -n _subnet_id=$subnet_id_ref
  {
    declare vpc_id; vpc_id=$(vpc-id-from-name $vpc)
    assert-ref-is-resource-id vpc_id vpc
    _subnet_id=$(subnet-id-from-vpc-id-and-avail-zone $vpc_id $avail_zone)
    if ref-is-empty _subnet_id; then
      _subnet_id=$(aws ec2 create-subnet --vpc-id $vpc_id --availability-zone $avail_zone --cidr-block $subnet_cidr_block --output text --query "Subnet.SubnetId" \
                      --tag-specifications $(tag-specs subnet Name $subnet) ) # subnet-<> # one default-subnet in every avail-zone
      assert-ref-is-resource-id _subnet_id subnet
      aws ec2 modify-subnet-attribute --subnet-id $_subnet_id --map-public-ip-on-launch
    else
      echo "# subnet already exists with same name $subnet" 1>&2
    fi
  } 1>&2
} # aws-subnet-create-out()

function dict-cidr-block-from-vpc() {
  declare dict_ref=$1
  dict.add $dict_ref $(aws ec2 describe-vpcs --output text --query "Vpcs[].[Tags[].Value,[CidrBlock]]")
} # dict-cidr-block-from-vpc()

function aws-subnet-build() {
  declare desc_file=$1
  declare subnet_id=; aws-subnet-build-out $desc_file subnet_id
} # aws-subnet-build()

function aws-subnet-build-out() {
  declare desc_file=$1 subnet_id_ref=$2
  desc_file=$(subnet-desc-file-from-dir $desc_file)
  declare subnet=; subnet-bname-out $desc_file subnet

  # where should we put these?
  declare default_region; default_region=$(default-region)
  declare default_subnet_avail_zone; default_subnet_avail_zone=$(first-avail-zone-in-region $default_region)
  declare default_vpc=vpc-0

  source "$VAARK_AWS_LIB_DIR/object-schema/object-schema-subnet-init.bash"

  subnet_vpc=$default_vpc
  subnet_avail_zone=$default_subnet_avail_zone

  if test -e "$desc_file"; then
    source   "$desc_file"
  fi

  declare -A resource_id_from_type=(); dict-resource-id-from-type resource_id_from_type $subnet

  declare -A cidr_block_from=(); dict-cidr-block-from-vpc cidr_block_from

  declare vpc_cidr_block=${cidr_block_from[$subnet_vpc]}

  declare vpc_subnet_mask=26 # fixfix

  declare subnet_cidr_block; subnet_cidr_block=$(subnet-ipv4-cidr-from-name $vm-$subnet_avail_zone $vpc_cidr_block $vpc_subnet_mask)

  # ! ref-is-empty __security_group && resource_id_from_type[security-group]=$__security_group

  # vpc
  declare vpc_id; vpc_id=$(vpc-id-from-name $subnet_vpc)
  assert-ref-is-resource-id vpc_id vpc

  # subnet
  aws-subnet-create-out $subnet $subnet_vpc $subnet_cidr_block $subnet_avail_zone $subnet_id_ref
} # aws-subnet-build-out()

function aws-vm-clone() {
  declare image=$1; shift; declare -a desc_files=( "$@" )
  vm-bname-inout image
  ! ref-is-empty image || die-dev "image may not be empty"
  ! aggr-ref-is-empty desc_files || usage "<image> <vm-desc-file> [ <vm-desc-file> ... ]"
  if test ${#desc_files[@]} -gt 1; then concur "$VAARK_AWS_BIN_DIR/vrk-aws-vm-clone $image" ${desc_files[@]}; return; fi # indirectly recursive
  declare desc_file=${desc_files[0]}
  desc_file=$(vm-desc-file-from-dir $desc_file)
  declare vm=; vm-bname-out $desc_file vm
  declare ssh_key_name=id_rsa

  # where should we put these?
  declare default_region; default_region=$(default-region)
  declare default_subnet_avail_zone; default_subnet_avail_zone=$(first-avail-zone-in-region $default_region)
  declare default_vpc=vpc-0
  declare default_subnet_base=$default_vpc-subnet-0
  declare default_subnet=$default_subnet_base-$default_subnet_avail_zone

  source "$VAARK_AWS_LIB_DIR/object-schema/object-schema-vm-init.bash"

  vm_avail_zone=$default_subnet_avail_zone
  vm_vpc=$default_vpc
  vm_subnets=( $default_subnet ) # default if not overriden by the following 'source .../<vm>.vm'

  if test -e "$desc_file"; then
    source   "$desc_file"
  fi

  declare default_type=t2.nano
  declare vm_type=${vm_type:-$default_type}

  vm-bname-inout vm_image
  if ! ref-is-empty vm_image && test $vm_image != $image; then
    die-dev "\$vm_image != \$image ($vm_image != $image)"
  fi
  vm_image=$image
  declare default_vm_vpc=$default_vpc
  declare vm_vpc=${vm_vpc:-$default_vm_vpc}

  declare image_id= image_user=
  read -r image_id  image_user <<< $(image-id-and-user-from-name $vm_image)

  # allow the user to just specify:
  #   vm_image=ami-0123456789abcdef
  #   vm_image_user=ubuntu
  if ref-is-empty image_id && ! ref-is-empty vm_image && ! ref-is-empty vm_image_user; then
    if [[ $vm_image =~ ^ami-[0-9a-f]+$ ]]; then
      declare image_id_exists_pcmd; image_id_exists_pcmd=$(image-id-exists-pcmd $vm_image)
      if $image_id_exists_pcmd; then
        image_id=$vm_image
        image_user=$vm_image_user
      fi
    fi
  fi

  ! ref-is-empty image_id   || die-dev "Cannot locate vm_image=$vm_image"
  ! ref-is-empty image_user || die-dev "Cannot locate vm_image_user from vm_image=$vm_image"

  if ! ref-is-empty vm_image_user && test $vm_image_user != $image_user; then
    die-dev "\$vm_image_user != \$image_user ($vm_image_user != $image_user)"
  fi
  vm_image_user=$image_user

  declare -A resource_id_from_type=(); dict-resource-id-from-type resource_id_from_type $vm

  if ! ref-is-empty resource_id_from_type[instance]; then
    echo "# vm already exists: $vm" 1>&2
    return
  fi

  declare subnet_id; subnet_id=$(subnet-id-from-name ${vm_subnets[0]}) # fixfix
  assert-ref-is-resource-id subnet_id subnet
  declare      vpc_id;    vpc_id=$(vpc-id-from-subnet-id $subnet_id)
  assert-ref-is-resource-id vpc_id vpc

  # security-group ssh-access
  declare security_group=$vm_vpc-sg-ssh-access
  declare security_group_id; security_group_id=$(aws-security-group-create $security_group $vm_vpc) # sg-<>
  ! ref-is-empty security_group_id || die "aws-security-group-create $security_group $vm_vpc"
  aws-security-group-add-ingress $security_group 22 tcp 0.0.0.0/0

  declare vm_key_file=${vm_key_file:-$(ssh-config-dir $vm)/$ssh_key_name}

  # copy ssh-keys from proto if they exists (not all protos were built locally)
  declare proto_ssh_key; proto_ssh_key=$(ssh-key-from-proto $vm_image)

  if ! ref-is-empty proto_ssh_key && test -e $proto_ssh_key; then
    mkdir -p $(dirname $vm_key_file)
    echo "#" \
         cp $proto_ssh_key     $vm_key_file 1>&2
         cp $proto_ssh_key     $vm_key_file
    echo "#" \
         cp $proto_ssh_key.pub $vm_key_file.pub 1>&2
         cp $proto_ssh_key.pub $vm_key_file.pub
  fi

  # generate key-pair if needed
  if ! test -e $vm_key_file || ! test -e $vm_key_file.pub; then
    rm -f $vm_key_file $vm_key_file.pub
    mkdir -p $(dirname $vm_key_file)
    ssh-keygen -q -N "" -t rsa -C $vm -f $vm_key_file
  fi
  chmod 0600 $vm_key_file
  chmod 0640 $vm_key_file.pub

  # import key-pair
  declare key_pair_id; key_pair_id=$(key-pair-id-from-name $vm)
  if ref-is-empty key_pair_id; then
    key_pair_id=$(aws ec2 import-key-pair --key-name $vm --public-key-material fileb://$vm_key_file.pub --output text --query "KeyPairId" \
                      --tag-specifications $(tag-specs key-pair Name $vm) ) # key-<>
  fi
  assert-ref-is-resource-id key_pair_id key

  # clone and start vm
  declare instance_id; instance_id=$(aws ec2 run-instances --count 1 --image-id $image_id --placement AvailabilityZone=$vm_avail_zone --instance-type $vm_type --key-name $vm --security-group-ids $security_group_id --subnet-id $subnet_id --output text --query "Instances[].InstanceId" \
                                         --tag-specifications $(tag-specs instance Name $vm) ) # i-<>
  assert-ref-is-resource-id instance_id i
  declare t=$SECONDS
  __msg="# $vm.vm: instance-status-ok: wait..." aws ec2 wait instance-status-ok --instance-ids $instance_id
  echo  "# $vm.vm: instance-status-ok: wait...done:" $(duration-str $(( $SECONDS - $t )) ) 1>&2

  declare root_volume_id; root_volume_id=$(root-volume-id-from-instance-id $instance_id)
  assert-ref-is-resource-id root_volume_id vol
  resource-id-add-name $root_volume_id $vm

  # this is only needed for images that are imported
  # need to do more research/experimentation to understand why
  # this may not be the best way to handle this
  declare device_name; device_name=$(aws ec2 describe-instances --instance-ids $instance_id --output text --query "Reservations[].Instances[].BlockDeviceMappings[0].DeviceName")
  aws ec2 modify-instance-attribute --instance-id $instance_id --block-device-mappings DeviceName="$device_name",Ebs="{DeleteOnTermination=true}"

  # generate ssh config file
  ssh-config-init $vm $vm_image_user $vm_key_file $instance_id

  declare desc_file_dir=$(dirname $desc_file)
  declare drive
  for drive in ${vm_drives[@]:-}; do
    drive=${drive%.drive}
    aws-drive-create    $desc_file_dir/$drive.drive
    aws-vm-drive-attach $vm $drive
  done

  if true; then # this is optional
    declare -A resource_id_from_type=(); dict-resource-id-from-type resource_id_from_type $vm
    mkdir -p $vaark_aws_vms_dir/$vm
    declare str; str=$(declare -p resource_id_from_type)

    # optional (this is NOT a general solution)
    str=$( str=${str/=(/=( }; str=${str// [/\\n  [}; str=${str/ )/\\n)}; echo -e "$str" )

    echo  "$str" > \
              $vaark_aws_vms_dir/$vm/resource-id-from-type.bash
    if ssh -q $vm true; then
      scp -q  $vaark_aws_vms_dir/$vm/resource-id-from-type.bash $vm:~
    fi
  fi
} # aws-vm-clone()

# https://docs.aws.amazon.com/vpc/latest/userguide/vpc-subnets-commands-example.html
function aws-subnet-destroy() {
  declare subnet=$1
  declare subnet_id; subnet_id=$(subnet-id-from-name $subnet)
  ref-is-empty subnet_id && return
  assert-ref-is-resource-id subnet_id subnet
  declare vpc_id; vpc_id=$(vpc-id-from-subnet-id $subnet_id)
  assert-ref-is-resource-id vpc_id vpc

  # subnet
  if ! ref-is-empty subnet_id; then
    declare -a instance_ids; instance_ids=( $(aws ec2 describe-instances --output text --query "Reservations[].Instances[].InstanceId" --filter Name=subnet-id,Values=$subnet_id) )
    if aggr-ref-is-empty instance_ids; then
      aws ec2 delete-subnet --subnet-id $subnet_id
    fi
  fi
  declare -A resource_id_from_type=(); dict-resource-id-from-type resource_id_from_type $subnet

  # route-table
  if ! ref-is-empty resource_id_from_type[route-table]; then
    declare -a subnet_ids; subnet_ids=( $(aws ec2 describe-route-tables --output text --query "RouteTables[].Associations[].SubnetId" --filters Name=association.route-table-id,Values=${resource_id_from_type[route-table]}) )
    if aggr-ref-is-empty subnet_ids; then
      aws ec2 delete-route-table --route-table-id ${resource_id_from_type[route-table]}
    fi
  fi

  # internet-gateway
  if ! ref-is-empty resource_id_from_type[internet-gateway]; then
    declare -a subnet_ids; subnet_ids=( $(aws ec2 describe-route-tables --output text --query "RouteTables[].Associations[].SubnetId" --filters Name=route-table-id,Values=${resource_id_from_type[internet-gateway]}) )
    if aggr-ref-is-empty subnet_ids; then
      if ! ref-is-empty vpc_id; then
        aws ec2 detach-internet-gateway --internet-gateway-id ${resource_id_from_type[internet-gateway]} --vpc-id $vpc_id
      fi
      aws   ec2 delete-internet-gateway --internet-gateway-id ${resource_id_from_type[internet-gateway]}
    fi
  fi
} # aws-subnet-destroy()

function aws-vm-destroy() {
  declare -a drives=( "$@" )
  test ${#drives[@]} -eq 0 && return

  if test ${#drives[@]} -gt 1; then concur $VAARK_AWS_BIN_DIR/vrk-aws-vm-destroy ${drives[@]}; return; fi # indirectly recursive
  declare vm=${drives[0]}
  vm-bname-inout vm
  declare instance_id;               instance_id=$(resource-id-from-type-and-name instance $vm)
  if ! ref-is-empty instance_id; then
    assert-ref-is-resource-id instance_id i
    declare instance_id_exists_pcmd; instance_id_exists_pcmd=$(instance-id-exists-pcmd $instance_id)
    if $instance_id_exists_pcmd; then
      declare -a volume_ids; volume_ids=( $(non-root-volume-ids-from-instance-id $instance_id) )
      declare volume_id drive
      for volume_id in ${volume_ids[@]}; do
        drive=$(resource-name-from-type-and-id volume $volume_id)
        if ! ref-is-empty drive; then
          echo "# $vm.vm: $drive.drive: detaching" 1>&2
          aws-vm-drive-detach $vm $drive
        else
          echo "# warning: $vm.vm: $drive.drive: unable to detach" 1>&2
        fi
      done
      declare root_volume_id; root_volume_id=$(root-volume-id-from-instance-id $instance_id)
      assert-ref-is-resource-id root_volume_id vol

      # get list of security-groups here
      declare -a security_groups; security_groups=( $(aws ec2 describe-instances --instance-id $instance_id --output text --query "Reservations[].Instances[].NetworkInterfaces[].Groups[].GroupName") )

      # vm
      declare previous_state current_state
      read -r previous_state current_state <<< $(aws ec2 terminate-instances --instance-ids $instance_id --output text --query "TerminatingInstances[].[PreviousState.Name,CurrentState.Name]")
      if test "$previous_state" != terminated; then
        declare t=$SECONDS
        echo  "# $vm.vm: terminate-instances: $previous_state -> $current_state" 1>&2
        __msg="# $vm.vm: instance-terminated: wait..." aws ec2 wait instance-terminated --instance-ids $instance_id
        echo  "# $vm.vm: instance-terminated: wait...done:" $(duration-str $(( $SECONDS - $t )) ) 1>&2
      fi
    fi
    aws ec2 delete-tags --resources $instance_id    # do this now, because it takes an hour for a terminated vm to be removed
    if ! ref-is-empty root_volume_id; then
      aws ec2 delete-tags --resources $root_volume_id # does not auto-delete
    fi
  fi

  # key-pair
  declare key_pair_id; key_pair_id=$(resource-id-from-type-and-name key-pair $vm)
  if ! ref-is-empty key_pair_id; then
    assert-ref-is-resource-id key_pair_id key
    aws ec2 delete-key-pair --key-pair-id $key_pair_id
  fi
  rm -fr $(ssh-config-dir $vm)
} # aws-vm-destroy()

# we need to omit the root drive which is always first
function non-root-volume-devices-max() { declare chr; for chr in {b..z}; do printf "/dev/xvd%s\n" $chr; done; }

function aws-vm-drive-attach() {
  declare vm=$1; shift; declare -a drives=( "$@" )
  vm-bname-inout vm
  declare instance_id; instance_id=$(resource-id-from-type-and-name instance $vm)
  ! ref-is-empty instance_id || die "No such vm: $vm"
  assert-ref-is-resource-id instance_id i

  declare -a volume_ids=()
  declare i drive volume_id
  for (( i=0; $i < ${#drives[@]}; i++ )); do
    drive=${drives[$i]}
    volume_id=$(resource-id-from-type-and-name volume $drive)
    if ! ref-is-empty volume_id; then
      assert-ref-is-resource-id volume_id vol
      volume_ids+=( $volume_id )
    else
      die "No such drive: $drive"
    fi
  done
  drives=( ${drives[@]} )
  test ${#drives[@]} -eq ${#volume_ids[@]} || die-dev "assert"

  declare -a volume_devices;        volume_devices=( $(non-root-volume-devices-max) )
  declare -a volume_devices_in_use; volume_devices_in_use=( $(non-root-volume-devices-from-instance-id $instance_id) )
  test ${#volume_devices[@]} -gt ${#volume_devices_in_use[@]} || die "too many volume-devices"

  declare i volume_device volume_device_in_use
  for (( i=0; $i < ${#volume_devices_in_use[@]}; i++ )); do
           volume_device=${volume_devices[$i]}
    volume_device_in_use=${volume_devices_in_use[$i]}
    test $volume_device == $volume_device_in_use || die "unexpected volume-device type in use: $volume_device_in_use"
    unset volume_devices[$i]
  done
  volume_devices=( ${volume_devices[@]} )
  test ${#drives[@]} -le ${#volume_devices[@]} || die "too many drives"
  declare i volume_id drive
  for (( i=0; $i < ${#volume_ids[@]}; i++ )); do
    volume_device=${volume_devices[$i]}
    volume_id=${volume_ids[$i]}
    drive=${drives[$i]}
    echo "# $vm.vm: $drive.drive: attaching" 1>&2
    aws ec2 attach-volume --instance-id "$instance_id" --device "$volume_device" --volume-id "$volume_id" --output json | tee /tmp/attach-volume.output.json
  done
} # aws-vm-drive-attach()

function aws-vm-drive-detach() {
  declare vm=$1; shift; declare -a drives=( "$@" )
  test ${#drives[@]} -eq 0 && return
  vm-bname-inout vm

  if test ${#drives[@]} -gt 1; then concur "$VAARK_AWS_BIN_DIR/vrk-aws-vm-drive-detach $vm" ${drives[@]}; return; fi # indirectly recursive
  declare drive=${drives[0]}
  drive-bname-inout drive
  declare instance_id; instance_id=$(resource-id-from-type-and-name instance $vm)
  ! ref-is-empty instance_id || die "No such vm: $vm"
  assert-ref-is-resource-id instance_id i
  declare -a attached_volume_ids; attached_volume_ids=$(attached-non-root-volume-ids-from-instance-id $instance_id)
  declare -A attached_volume_ids_set=(); dict.add attached_volume_ids_set $(printf "%s $instance_id\n" ${attached_volume_ids[@]})

  declare volume_id; volume_id=$(resource-id-from-type-and-name volume $drive)
  if ! ref-is-empty volume_id; then
    assert-ref-is-resource-id volume_id vol
    if test -v attached_volume_ids_set[$volume_id]; then
      ### first unmount drive!!!
      echo "MUST UNMOUNT VOLUME HERE" 1>&2
      echo "# $vm.vm: $drive.drive: detaching" 1>&2
      aws ec2 detach-volume --volume-id $volume_id --instance-id $instance_id --output json | tee /tmp/detach-volume.outputjson
    else
      : # silently ignore when the drive is not attached (idempotent)
    fi
  fi
} # aws-vm-drive-detach()

function aws-image-import() {
  declare -a canon_desc_files=( "$@" )
  aggr-ref-is-empty canon_desc_files && return
  if test ${#canon_desc_files[@]} -gt 1; then concur $VAARK_AWS_BIN_DIR/vrk-aws-image-import ${canon_desc_files[@]}; return; fi # indirectly recursive
  declare canon_desc_file=${canon_desc_files[0]}

  declare vm=; vm-bname-out $canon_desc_file vm
  declare region=${__region:-$(default-region)}

  if ! test -e "$canon_desc_file"; then
    canon_desc_file=~/.vaark/vbox/vms/$vm/vaark/$vm.vm
    test -e $canon_desc_file || die "No such vm: $vm"
  fi
  source "$canon_desc_file"

  declare user=${vm_basevm_user:-}
  declare ssh_key=
  for ssh_key in ${vm_basevm_ssh_keys[@]}; do
    test -e $ssh_key && break
    ssh_key=
  done
  vrk-vm-stop $vm

  declare format=OVA
  declare format_ext=ova
  declare tmp_dir=/tmp/$USER-$vm
  mkdir -p $tmp_dir
  rm -f    $tmp_dir/$vm.$format_ext

  declare t=$SECONDS
  echo "# $vm.vm: vrk-vm-export: wait..." 1>&2
  vrk-vm-export $tmp_dir/$vm.$format_ext $vm
  echo "# $vm.vm: vrk-vm-export: wait...done:" $(duration-str $(( $SECONDS - $t )) ) 1>&2

  test -e  $tmp_dir/$vm.$format_ext || die "no such file $tmp_dir/$vm.$format_ext"
  declare user_id; user_id=$(aws sts get-caller-identity --output text --query UserId)
  assert-ref-is-user-id user_id
  declare import_bucket=$user_id-vaark-disk-image-file-bucket

  declare trust_policy_file=$VAARK_AWS_LIB_DIR/trust-policy.json
  test -e $trust_policy_file || die "no such file $trust_policy_file"

  declare      role_policy_file=$VAARK_AWS_LIB_DIR/role-policy.json
  test -e     $role_policy_file.in || die "no such file $role_policy_file.in"
  cp          $role_policy_file.in \
     $tmp_dir/role-policy.json
  sed -i~ -E \
      -e "s/@@import-bucket@@/$import_bucket/g" \
        $tmp_dir/role-policy.json
  rm -f $tmp_dir/role-policy.json~

  test -e $tmp_dir/role-policy.json || die "no such file $tmp_dir/role-policy.json"

  declare   role=vmimport
  declare policy=vmimport

  declare -A role_set=(); dict.add-keys role_set $(aws iam list-roles --output text --query "Roles[].RoleName")
  if ! test -v role_set[$role]; then
    aws iam create-role   --role-name $role           --assume-role-policy-document "file://$trust_policy_file" --tags Key=Name,Value=$role --output json | tee /tmp/create-role.output.json
  fi

  # "Adds or updates an inline policy document ..."
  aws iam put-role-policy --role-name $role --policy-name $policy --policy-document "file://$tmp_dir/role-policy.json" --output json | tee /tmp/put-role-policy.output.json
  rm -f $tmp_dir/role-policy.json

  declare -A bucket_set=(); dict.add-keys bucket_set $(aws s3api list-buckets --output text --query "Buckets[].Name")
  if ! test -v bucket_set[$import_bucket]; then
    aws s3 mb s3://$import_bucket --region $region
  fi

  declare key_pair_id; key_pair_id=$(key-pair-id-from-name $vm)
  if ref-is-empty key_pair_id; then
    declare key_pair_id; key_pair_id=$(aws ec2 import-key-pair --key-name $vm --public-key-material fileb://$ssh_key.pub --output text --query "KeyPairId" \
                                           --tag-specifications $(tag-specs key-pair Name $vm) ) # key-<>
    assert-ref-is-resource-id key_pair_id key
  fi

  declare key=vms/$vm/$vm.$format_ext # not ssh-key
  aws s3api delete-object                --bucket $import_bucket --key $key
  declare t=$SECONDS
  declare etag; etag=$(__msg="# $vm.vm: put-object: wait..." aws s3api put-object --bucket $import_bucket --key $key --body $tmp_dir/$vm.$format_ext --output text --query "ETag")
  ! ref-is-empty etag || die-dev "aws s3api put-object"
  echo  "# $vm.vm: put-object: wait...done:" $(duration-str $(( $SECONDS - $t )) ) 1>&2

  declare t=$SECONDS
  declare user_bucket; user_bucket={S3Bucket="$import_bucket",S3Key="$key"}
  declare import_task_id; import_task_id=$(__msg="# $vm.vm: import-image: wait..." aws ec2 import-image --description "vaark-built $key" --disk-containers Format=$format,UserBucket="$user_bucket" --output text --query "ImportTaskId")
  ! ref-is-empty import_task_id || die-dev "aws ec2 import-image"
  assert-ref-is-resource-id import_task_id import-ami

  declare status
  while true; do
    status=$(command aws ec2 describe-import-image-tasks --import-task-ids $import_task_id --output text --query "ImportImageTasks[].Status")
    case $status in
      (completed)
        break
        ;;
      (deleted)
        aws ec2 describe-import-image-tasks --import-task-ids $import_task_id 1>&2
        die "aws ec2 import-image"
        ;;
      (*)
        sleep 5
    esac
  done
  echo  "# $vm.vm: import-image: wait...done:" $(duration-str $(( $SECONDS - $t )) ) 1>&2
  declare image_id snapshot_id
  read -r image_id snapshot_id <<< $(aws ec2 describe-import-image-tasks --import-task-ids $import_task_id --output text --query "ImportImageTasks[].[ImageId,SnapshotDetails[].SnapshotId]" | flatten-stdin)
  resource-id-add-name $snapshot_id $vm
  resource-id-add-name $image_id    $vm
  image-id-add-user    $image_id    $user
  # aws ec2 wait image-available --image-ids $image_id
} # aws-image-import()

function aws-vm-import() {
  declare -a desc_files=( "$@" )
  aggr-ref-is-empty desc_files && return
  if test ${#desc_files[@]} -gt 1; then concur $VAARK_AWS_BIN_DIR/vrk-aws-vm-import ${desc_files[@]}; return; fi # indirectly recursive
  declare desc_file=${desc_files[0]}

  declare vm=; vm-bname-out $desc_file vm
  aws-image-import $vm
  aws-vm-build $desc_file
} # aws-vm-import

function aws-vm-reboot() {
  declare -a vms=( "$@" )
  if ref-is-set __all; then vms=( $(aws-vm-show) ); fi
  aggr-ref-is-empty vms && return
  if test ${#vms[@]} -gt 1; then concur $VAARK_AWS_BIN_DIR/vrk-aws-vm-reboot ${vms[@]}; return; fi # indirectly recursive
  declare vm=${vms[0]}
  vm-bname-inout vm
  declare instance_id; instance_id=$(resource-id-from-type-and-name instance $vm)
  assert-ref-is-resource-id instance_id i
  declare public_ipv4_addr; public_ipv4_addr=$(public-ip-addr-from-instance-id $instance_id)
  declare ssh_port=${__ssh_port:-22}
  aws ec2 reboot-instances --instance-ids $instance_id
  ref-is-set __async && return
  declare t=$SECONDS
  echo "# $vm.vm: reboot-instances: wait..." 1>&2
  while socket-is-listening $ssh_port $public_ipv4_addr; do sleep 1; done
  until socket-is-listening $ssh_port $public_ipv4_addr; do sleep 1; done
  until ssh -p $ssh_port -o "ConnectTimeout = 10" -o "ConnectionAttempts = 10" $vm true; do sleep 1; done
  echo "# $vm.vm: reboot-instances: wait...done:" $(duration-str $(( $SECONDS - $t )) ) 1>&2
} # aws-vm-reboot()

function aws-vm-rebuild() {
  declare -a vms=( "$@" )
  echo  "# vrk-aws-vm-destroy" "${vms[@]}" 1>&2
  aws-vm-destroy  "${vms[@]}"
  echo  "# vrk-aws-vm-build"   "${vms[@]}" 1>&2
  aws-vm-build    "${vms[@]}"
} # aws-vm-rebuild()

function aws-vm-reclone() {
  declare image=$1; shift; declare -a vms=( "$@" )
  echo  "# vrk-aws-vm-destroy" "${vms[@]}" 1>&2
  aws-vm-destroy      "${vms[@]}"
  echo  "# vrk-aws-vm-clone"   "${vms[@]}" 1>&2
  aws-vm-clone $image "${vms[@]}"
} # aws-vm-reclone()

function aws-vm-restart() {
  declare -a vms=( "$@" )
  echo  "# vrk-aws-vm-stop"  "${vms[@]}" 1>&2
  aws-vm-stop   "${vms[@]}"
  echo  "# vrk-aws-vm-start" "${vms[@]}" 1>&2
  aws-vm-start  "${vms[@]}"
} # aws-vm-restart()

function aws-vm-ssh() {
  declare vm=$1
  vm-bname-inout vm
  aws-vm-start $vm
  ssh "$@"
} # aws-vm-ssh()

function aws-vm-start() {
  declare -a vms=( "$@" )
  if ref-is-set __all; then vms=( $(aws-vm-show) ); fi
  aggr-ref-is-empty vms && return
  if test ${#vms[@]} -gt 1; then concur $VAARK_AWS_BIN_DIR/vrk-aws-vm-start ${vms[@]}; return; fi # indirectly recursive
  declare vm=${vms[0]}
  vm-bname-inout vm
  declare instance_id; instance_id=$(resource-id-from-type-and-name instance $vm)
  assert-ref-is-resource-id instance_id i
  declare previous_state current_state
  read -r previous_state current_state <<< $(aws ec2 start-instances --instance-id $instance_id --output text --query "StartingInstances[].[PreviousState.Name,CurrentState.Name]")
  #ref-is-set __async && return # unable to use this code because of the use of ssh-config-update below
  if test "$previous_state" != running; then
    declare t=$SECONDS
    echo  "# $vm.vm: start-instances: $previous_state -> $current_state" 1>&2
    __msg="# $vm.vm: instance-running: wait..." aws ec2 wait instance-running --instance-ids $instance_id
    echo  "# $vm.vm: instance-running: wait...done:" $(duration-str $(( $SECONDS - $t )) ) 1>&2

    ssh-config-update $vm
  fi
} # aws-vm-start()

function aws-vm-stop() {
  declare -a vms=( "$@" )
  if ref-is-set __all; then vms=( $(aws-vm-show) ); fi
  aggr-ref-is-empty vms && return
  if test ${#vms[@]} -gt 1; then concur $VAARK_AWS_BIN_DIR/vrk-aws-vm-stop ${vms[@]}; return; fi # indirectly recursive
  declare vm=${vms[0]}
  vm-bname-inout vm
  declare instance_id; instance_id=$(resource-id-from-type-and-name instance $vm)
  assert-ref-is-resource-id instance_id i
  declare previous_state current_state
  read -r previous_state current_state <<< $(aws ec2 stop-instances --instance-id $instance_id --output text --query "StoppingInstances[].[PreviousState.Name,CurrentState.Name]")
  ref-is-set __async && return
  if test "$previous_state" != stopped; then
    declare t=$SECONDS
    echo  "# $vm.vm: stop-instances: $previous_state -> $current_state" 1>&2
    __msg="# $vm.vm: instance-stopped: wait..." aws ec2 wait instance-stopped --instance-ids $instance_id
    echo  "# $vm.vm: instance-stopped: wait...done:" $(duration-str $(( $SECONDS - $t )) ) 1>&2
  fi
} # aws-vm-stop()

function aws-image-destroy() {
  declare -a images=( "$@" )
  aggr-ref-is-empty images && return
  if test ${#images[@]} -gt 1; then concur $VAARK_AWS_BIN_DIR/vrk-aws-image-destroy ${images[@]}; return; fi # indirectly recursive
  declare image=${images[0]}
  declare -a image_ids; image_ids=( $(resource-id-from-type-and-name image $image) )
  declare image_id
  for image_id in ${image_ids[@]}; do
    assert-ref-is-resource-id image_id
    declare snapshot_id; snapshot_id=$(aws ec2 describe-images --image-ids $image_id --output text --query "Images[].BlockDeviceMappings[].Ebs.SnapshotId")
    assert-ref-is-resource-id snapshot_id snapshot
    aws ec2 deregister-image --image-id  $image_id
    aws ec2 delete-tags      --resources $image_id
    if ! ref-is-empty snapshot_id; then
      aws ec2 delete-snapshot --snapshot-id $snapshot_id
      aws ec2 delete-tags     --resources   $snapshot_id
    fi
  done
  declare key_pair_id; key_pair_id=$(resource-id-from-type-and-name key-pair $image)
  ref-is-empty key_pair_id || aws ec2 delete-key-pair --key-pair-id $key_pair_id
} # aws-image-destroy()

function dict-show() {
  declare dict_func=$1
  declare -A dict=(); $dict_func dict
  test ${#dict[@]} -eq 0 && return
  if     ref-is-set __keys && ! ref-is-set __vals; then
    dict.flatten dict | cut -d " " -f 1 # only field 1
  elif ! ref-is-set __keys &&   ref-is-set __vals; then
    declare line
    dict.flatten dict | tr -s " " | cut -d " " -f 2- # squeeze multiple spaces into single space, field 2 until EOL
  else # neither or both __keys and __vals
    dict.flatten dict
  fi
} # dict-show()

function aws-drive-show() {
  dict-show dict-volume-id-from-name
} # aws-drive-show()

function aws-subnet-show() {
  dict-show dict-subnet-id-from-name
} # aws-subnet-show()

function aws-image-show() {
  dict-show dict-image-id-from-name
} # aws-image-show()

function aws-vm-show() {
  dict-show dict-instance-id-from-name
} # aws-vm-show()
