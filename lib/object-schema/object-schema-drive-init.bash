# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

declare -- drive_avail_zone= # defaults to first availability-zone (ordered lexically) in drive_region
declare -- drive_region= # defaults to users default region
declare -- drive_size= # defaults to 100 MiB
