# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-

declare -- vm_avail_zone=
declare -- vm_image=
declare -- vm_image_user=
declare -- vm_type=
declare -- vm_vpc=  # defaults to vpc-0
declare -A vm_security_groups=()
declare -a vm_basevm_ssh_keys=()
declare -a vm_drives=()
declare -a vm_subnets=() # defaults to vpc-0-subnet-0-<first-avail-zone>
