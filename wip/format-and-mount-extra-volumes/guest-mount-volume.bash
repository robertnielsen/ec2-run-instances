#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#shopt -s lastpipe
export PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME:-main}(): '
declare source_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
declare -r -g -x VAARK_AWS_BIN_DIR=$source_dir
declare -r -g -x VAARK_AWS_LIB_DIR=$VAARK_AWS_BIN_DIR/../lib

source "$VAARK_AWS_LIB_DIR/utils.bash"

function guest-mount-drive() {
  set -o xtrace
  declare canon_desc_file=$1
  source "$canon_desc_file"

  ### RUN ON GUEST ###
  declare volume_mount_device="/dev/xvfb"
  declare drive_mount_point="/data/drive-1"
  declare volume_mount_filesystem_type="ext4"
  declare volume_mount_options="defaults,nofail" # are these the best defaults ???

  #while ! test -e $mount_device; do sleep 1; done

  mkfs -t $volume_mount_filesystem_type $volume_mount_device
  declare dump=0 pass=0 # are these the best defaults ???
  declare line; line="$mount_device  $mount_point  $mount_filesystem_type  $mount_options  $dump  $pass # generated-by-vaark-aws"
  echo "$line" >> /etc/fstab
  mkdir -p "$mount_point"
  mount    "$mount_point"
}

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  guest-mount-drive "$@"
fi

# ssh into host with two volumes
#
## make the mount points
# sudo mkdir -p /data/drive-1
# sudo mkdir -p /data/drive-2
#
## determine the devices (remember there is also the boob/root device)
# find /dev -type b | sort -V
#   by inspection you can deduce
#     drive-1: /dev/nvme1n1
#     drive-2: /dev/nvme2n1
#
## make filesystems on the devices
# sudo mkfs -t ext4 /dev/nvme1n1
# sudo mkfs -t ext4 /dev/nvme2n1
#
## mount the devices on the mount-points
# sudo mount /dev/nvme1n1 /data/drive-1
# sudo mount /dev/nvme2n1 /data/drive-2
