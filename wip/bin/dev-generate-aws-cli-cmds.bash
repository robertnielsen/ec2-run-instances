#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#shopt -s lastpipe
export PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME:-main}(): '
declare source_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

function start() {
  declare -a cmds; cmds=( ec2 s3api )

  rm -fr   ~/bin/aws/

  mkdir -p ~/bin/aws/
  declare cmd
  for cmd in ${cmds[@]}; do
    declare -a subcmds; subcmds=( $(aws $cmd help | while read line; do if [[ $line =~ [[:cntrl:]]o[[:blank:]]([a-z0-9-]+) ]]; then test ${BASH_REMATCH[1]} != help && echo ${BASH_REMATCH[1]}; fi; done) )
    declare subcmd
    for subcmd in ${subcmds[@]}; do
      declare name=aws-$cmd-$subcmd
      declare -p name 1>&2
      {
        echo "#!/usr/bin/env bash"
        echo "aws $cmd $subcmd \"$@\""
      } >        ~/bin/aws/$name
      chmod 0755 ~/bin/aws/$name
    done
  done
  ls -l ~/bin/aws/
}

if test "$0" == "${BASH_SOURCE[0]:-$0}"; then
  start "$@"
fi
