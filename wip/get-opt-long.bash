#!/usr/bin/env bash
# -*- mode: sh; sh-basic-offset: 2; indent-tabs-mode: nil -*-
set -o errexit -o nounset -o pipefail
set -o errtrace
shopt -s inherit_errexit 2> /dev/null || { echo "$0: error: bash 4.4 or later required" 1>&2; exit 1; }
#shopt -s lastpipe
export PS4='+(${BASH_SOURCE[0]:-$0}:${LINENO}): ${FUNCNAME:-main}(): '
declare source_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

source "$source_dir/../lib/utils.bash"

function extract-opt-with-rhs-usage() {
  declare opts_ref=$1 msg=${2:-}
  declare -n _opts=$opts_ref
  declare progname; progname=$(basename $0)
  declare -a out=()
  declare lhs rhs
  for lhs in ${!_opts[@]}; do rhs=${_opts[$lhs]}; test $rhs == ""  && out+=( "--$lhs"    ); done
  for lhs in ${!_opts[@]}; do rhs=${_opts[$lhs]}; test $rhs == ":" && out+=( "--$lhs=<>" ); done
  {
    ! ref-is-empty msg && echo "$progname: error: $msg"
    echo "usage: $progname: ${out[*]}"
  } 1>&2
  exit 1
} # extract-opt-with-rhs-usage()

function convert-opts-str-out() {
  declare opts_str=$1 opts_ref=$2
  declare -n _opts=$opts_ref
  declare -a args=( ${opts_str//,/ } )
  for arg in ${args[@]}; do
    if [[ $arg =~ ^(.+)(:)$ ]]; then # opt-spec
      _opts[${BASH_REMATCH[1]}]=${BASH_REMATCH[2]}
    else
      _opts[$arg]= # opt-spec
    fi
  done
} # convert-opts-str-out()

function extract-opt-with-rhs-out() {
  declare args_ref=$1 opts_in_ref=$2 opts_out_ref=$3
  declare -n _args=$args_ref _opts_in=$opts_in_ref _opts_out=$opts_out_ref
  test ${#_args[@]} -eq 0 && return
  declare -a unset_args=()
  declare i
  for (( i=0; $i < ${#_args[@]}; i++ )); do
    if [[ ${_args[$i]} =~ ^--([a-z][a-z0-9-]+) ]]; then
      declare opt=${BASH_REMATCH[1]}
      test "$opt" != help || extract-opt-with-rhs-usage $opts_in_ref
      if test -v _opts_in[$opt]; then
        declare rhs
        declare has_rhs=0; test "${_opts_in[$opt]}" == ":" && has_rhs=1
        if [[ ${_args[$i]} =~ ^--$opt=(.*)$ ]]; then
          declare cap_rhs=${BASH_REMATCH[1]}
          if   ref-is-set has_rhs; then
            rhs=$cap_rhs
          elif ref-is-empty cap_rhs; then # so --verbose= is the same as --verbose=0
            rhs=0
          elif [[ $cap_rhs =~ ^(0|1)$ ]]; then
            rhs=$cap_rhs
          else
            extract-opt-with-rhs-usage $opts_in_ref "unknown right-hand-side for option --$opt=$cap_rhs (expected 0 or 1)"
          fi
          unset_args=( ${unset_args[@]} $i ) # reverse order
        elif [[ ${_args[$i]} =~ ^--$opt$ ]]; then
          if ! ref-is-set has_rhs; then
            rhs=1
            unset_args=( ${unset_args[@]} $i ) # reverse order
          else
            unset_args=( ${unset_args[@]} $i ) # reverse order
            i=$(( $i + 1 ))
            test -v _args[$i] || extract-opt-with-rhs-usage $opts_in_ref "missing required right-hand-side for option --$opt"
            rhs=${_args[$i]}
            unset_args=( ${unset_args[@]} $i ) # reverse order
          fi
        fi
        _opts_out[$opt]=$rhs
      else
        extract-opt-with-rhs-usage $opts_in_ref "unknown option --$opt"
      fi
    fi
  done
  declare i; for i in ${unset_args[@]}; do unset _args[$i]; done
  _args=( "${_args[@]}" )
} # extract-opt-with-rhs-out()

function run-top-level-cmd-with-opts() {
  declare opts_str=$1; shift; declare -a cmd=( "$@" )
  declare -A OPTS_IN=(); convert-opts-str-out     $opts_str OPTS_IN
  declare -A OPTS=();    extract-opt-with-rhs-out cmd       OPTS_IN OPTS
  run-top-level-cmd "${cmd[@]}"
} # run-top-level-cmd-with-opts()

function some-top-level-cmd() { _opts_="silent,verbose,directory:,file:,three-blind-mice:"
  #declare -A opts_in=(); convert-opts-str-out $_opts_ opts_in; declare -p opts_in
  declare -p OPTS_IN
  declare -p OPTS
  echo $FUNCNAME "$@"
} # some-top-level-cmd()

## generated ##
run-top-level-cmd-with-opts "silent,verbose,directory:,file:,three-blind-mice:" some-top-level-cmd "$@"
## generated ##
