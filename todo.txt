import extra drives
import vms with extra drives
format and mount extra-drives on guest
create and add network interfaces

vrk-aws-security-group-create
vrk-aws-security-group-destroy

vrk-aws-vpc-create  # includes route-table & internet-gateway
vrk-aws-vpc-destroy # includes route-table & internet-gateway

vrk-aws-subnet-create
vrk-aws-subnet-destroy


aws s3api put-public-access-block --bucket <> --public-access-block-configuration BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true
aws s3api get-public-access-block --bucket <>

confirm user has default region

--region
--availability-zone

implement vrk-aws-subnet-build
implement vrk-aws-subnet-destroy

export AWS_DEFAULT_OUTPUT=json
export AWS_PAGER= # empty

declare config_file=${AWS_CONFIG_FILE:-$HOME/.aws/config}
declare credentials_file=${AWS_SHARED_CREDENTIALS_FILE:-$HOME/.aws/credentials}
declare region=${AWS_REGION:-${AWS_DEFAULT_REGION:-$(aws configure get region)}} # may be empty
