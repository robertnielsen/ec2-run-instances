# -*- mode: Makefile -*-

SHELL := bash -o nounset -o pipefail

opt-dir := ${HOME}/opt/vaark-aws

%-lr.dot.pdf : %.dot
	dot -Tpdf -Grankdir=LR -o $@ $<

%-tb.dot.pdf : %.dot
	dot -Tpdf -Grankdir=TB -o $@ $<

bin-files               :=
doc-graphics-files      :=
lib-files               :=
lib-object-schema-files :=
lib-template-files      :=

include Makefile-vars

.PHONY : all
.PHONY : install
.PHONY : doc
.PHONY : bin
.PHONY : lib
.PHONY : lib-object-schema
.PHONY : lib-templates
.PHONY : clean
.PHONY : uninstall

all : doc

install : all bin lib lib-object-schemas lib-templates

doc : ${doc-graphics-files}
	install -d ${opt-dir}/doc/graphics/
	install $^ ${opt-dir}/doc/graphics/

bin : ${bin-files}
	install -d ${opt-dir}/bin/
	install $^ ${opt-dir}/bin/

lib : ${lib-files}
	install -d ${opt-dir}/lib/
	install $^ ${opt-dir}/lib/

lib-object-schemas : ${lib-object-schema-files}
	install -d ${opt-dir}/lib/object-schema/
	install $^ ${opt-dir}/lib/object-schema/

lib-templates : ${lib-template-files}
	install -d ${opt-dir}/lib/templates/
	install $^ ${opt-dir}/lib/templates/

clean :
	rm -f ${doc-graphics-files}

uninstall :
	rm -fr ${opt-dir}
